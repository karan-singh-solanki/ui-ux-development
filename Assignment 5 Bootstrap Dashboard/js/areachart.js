google.charts.load('current', { 'packages': ['corechart'] });
google.charts.setOnLoadCallback(drawChart);

function drawChart() {
    var data = google.visualization.arrayToDataTable([
        ['Year', 'Sales', 'Expenses'],
        ['2013', 1000, 400],
        ['2014', 1170, 460],
        ['2015', 660, 1120],
        ['2016', 1030, 540]
    ]);

    var options = {
        chartArea: { 'width': '100%', 'height': '100%' },
        hAxis: {
            baselineColor: 'none',
            ticks: []
        },
        legend: { position: 'none' },

        vAxis: {
            minValue: 0,
            baselineColor: '#fff',
            gridlineColor: '#fff',
            ticks: []
        },
        height: 220,
        lineWidth: 0,
        colors: ["#4fb6e3", "#4fb6e3"]
    };

    var chart = new google.visualization.AreaChart(document.getElementById('chart_div'));
    chart.draw(data, options);
}