google.charts.load('current', { 'packages': ['line'] });
google.charts.setOnLoadCallback(drawChart);

function drawChart() {

    var data = new google.visualization.DataTable();
    data.addColumn('number', 'Day');
    data.addColumn('number', 'Guardians of the Galaxy');
    data.addColumn('number', 'The Avengers');
    data.addColumn('number', 'Transformers: Age of Extinction');

    data.addRows([
        [1, 37.8, 80.8, 20.8],
        [2, 30.9, 69.5, 26.4],
        [3, 25.4, 57, 34.7],
        [4, 11.7, 18.8, 15.5],
        [5, 11.9, 17.6, 10.4],
        [6, 8.8, 13.6, 11.7],
        [7, 7.6, 12.3, 9.6],
        [8, 12.3, 29.2, 10.6],
        [9, 16.9, 42.9, 14.8],
        [10, 12.8, 30.9, 11.6],
        [11, 5.3, 7.9, 4.7],
        [12, 6.6, 8.4, 5.2],
        [13, 5.8, 6.3, 3.6],
        [14, 4.2, 6.2, 5.4]
    ]);

    var options = {
        width: 600,
        height: 350,
        axes: {
            x: {
                0: { side: 'none' }
            }
        },
        legend: { position: 'none' },
        colors: ["#616be8", "#40a4f1", "#c1c5e2"],
    };

    var chart = new google.charts.Line(document.getElementById('line_top_x'));

    chart.draw(data, google.charts.Line.convertOptions(options));
}