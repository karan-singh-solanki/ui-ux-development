google.charts.load("current", { packages: ["corechart"] });
google.charts.setOnLoadCallback(drawChart);

function drawChart() {
    var data = google.visualization.arrayToDataTable([
        ['Task', 'Hours per Day'],
        ['Canada', 11],
        ['USA', 2],
        ['London', 7]
    ]);

    var options = {
        chartArea: { 'width': '95%', 'height': '80%', },
        height: 350,
        pieHole: 0.6,
        legend: { position: 'top', alignment: 'center', },
        colors: ["#616be8", "#40a4f1", "#c1c5e2"],

    };

    var chart = new google.visualization.PieChart(document.getElementById('donutchart'));
    chart.draw(data, options);
}