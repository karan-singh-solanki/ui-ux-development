export class Pass {
    flagPricing: boolean;
    vehicleType: string;
    selectType: string;
    currency: string;
    template:string;

    constructor() {
        this.flagPricing = false;
        this.currency = "dollar";
        this.template = `<!--Cycle-->
        <!-- <span id="cycle-span-final-price" class="final-price"></span> -->
        <div class="vehicle-type" id="cycle-div">
            <h3>CYCLE</h3>
            <div class="circle center-flex">
                <label id="cycle-price" class="label-month-price">$100</label>
                <label id="cycle-duration">/month</label>
            </div>
            <select id="cycle-currency" class="custom-input change-curr">
                <option value="dollar" selected>$ Dollar</option>
                <option value="yen">&#165; YEN</option>
            </select>
            <div class="labels-price center-flex">
                <select id="cycle" class="custom-input purchase-curr">
                    <option value="none" selected disabled hidden>
                        Select Plan
                    </option>
                    <option data-price="5" , data-duration="daily">Daily</option>
                    <option data-price="100" , data-duration="monthly">Monthly</option>
                    <option data-price="500" , data-duration="yearly">Yearly</option>
                </select>
            </div>
            <button id="cycle-button" type="button">GET PASS</button>
        </div>

        <!--Motor Cycle-->
        <!-- <span id="motor-cycle-span-final-price" class="final-price"></span> -->
        <div class="vehicle-type" id="motor-cycle-div">
            <h3>MOTOR CYCLE</h3>
            <div class="circle center-flex">
                <label id="motor-cycle-price" class="label-month-price">$200</label>
                <label id="motor-cycle-duration">/month</label>
            </div>
            <div class="labels-price center-flex">
                <select id="motor-cycle-currency" class="custom-input change-curr">
                    <option value="dollar" selected>$ Dollar</option>
                    <option value="yen">¥ YEN</option>
                </select>

                <select id="motor-cycle" class="custom-input purchase-curr">
                    <option value="none" selected disabled hidden>
                        Select Plan
                    </option>
                    <option data-price="10" , data-duration="daily">Daily</option>
                    <option data-price="200" , data-duration="monthly">Monthly</option>
                    <option data-price="1000" , data-duration="yearly">Yearly</option>
                </select>
            </div>
            <button id="motor-cycle-button" type="button">GET PASS</button>
        </div>

        <!--Four Wheeler-->
        <!-- <span id="four-wheeler-final-price" class="final-price"></span> -->
        <div class="vehicle-type" id="four-wheeler-div">
            <h3>FOUR WHEELER</h3>
            <div class="circle center-flex">
                <label id="four-wheeler-price" class="label-month-price">$500</label>
                <label id="four-wheeler-duration">/month</label>
            </div>
            <select id="four-wheeler-currency" class="custom-input change-curr">
                <option value="dollar" selected>$ Dollar</option>
                <option value="yen">&#165; YEN</option>
            </select>
            <div class="labels-price center-flex">
                <select id="four-wheeler" class="custom-input purchase-curr">
                    <option value="none" selected disabled hidden>
                        Select Plan
                    </option>
                    <option data-price="20" , data-duration="daily">Daily</option>
                    <option data-price="400" , data-duration="monthly">Monthly</option>
                    <option data-price="2000" , data-duration="yearly">Yearly</option>
                </select>
            </div>

            <button id="four-wheeler-button" type="button">GET PASS</button>
        </div>`;
    }

    initializePass(vType: string) {
        this.vehicleType = vType;
        let vehicleDiv = document.getElementById("pricing-list");
        vehicleDiv.innerHTML = this.template;
        let parent = this;

        let changeCurr = function(): void {
            if (this.currency == ("dollar"))
                this.currency = "yen";
            else
                this.currency = "dollar";
        };

        document.querySelectorAll('.change-curr').forEach(item => {
            item.addEventListener('change', changeCurr)
        })

        document.querySelectorAll('.purchase-curr').forEach(item => {
            item.addEventListener("change", _ => {
                var id = item.id;
                var currencySymbol = parent.currency == "dollar" ? "$" : '¥';

                var selectedIndex = (document.getElementById(id) as HTMLSelectElement).selectedIndex;
                var selectedOption = (document.getElementById(id) as HTMLSelectElement).options[selectedIndex];

                var price = parseInt(selectedOption.dataset.price);
                if (currencySymbol != "$")
                    price = price / 2;

                document.getElementById(`${id}-price`).textContent = `${currencySymbol}${price}`;
                document.getElementById(`${id}-duration`).textContent = `/${selectedOption.dataset.duration}`;

                var btn = document.getElementById(`${id}-button`);
                btn.style.display = "block";

                btn.onclick = function () {
                    document.getElementById(`${id}-div`).style.display = 'none';
                    document.getElementById("pricing-list").innerHTML = `<h2> Final Price:  $ ${selectedOption.dataset.price} </h2>`;
                };
            })
        })
        this.pricingFunction();
    }


    pricingFunction() {
        document.getElementById('menu-checkbox-pricing').click();

        var vehicles = document.querySelectorAll(`.vehicle-type`) as NodeListOf<HTMLElement>;
        for (let i = 0; i < vehicles.length; i++) {
            if (vehicles[i] != document.getElementById(`${this.vehicleType}-div`))
                vehicles[i].style.display = "none"; // Set display none to hide all inputs
        }
        (document.getElementById(`${this.vehicleType}-button`)as HTMLButtonElement).style.display = "none";
    }
}