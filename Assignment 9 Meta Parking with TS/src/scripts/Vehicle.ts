import { Pass } from "./Pass";
import { toggleAlert } from "./app";

export class Vehicle {

    formFields: NodeListOf<HTMLElement>;
    formBtn: HTMLElement;
    vehicleType: string;
    template: string;
    pass: Pass;

    static flagVehicleForm: boolean = false;

    constructor() {
        this.template = `
            <form id="vehicle-form">
            <span id="vehicle-input-heading">Can we know about your vehicle?</span>
            <input name="vehicle make" class="custom-input" type="text" placeholder="Vehicle Make (Company)"
                required />
            <input name="vehicle model" class="custom-input" type="text" placeholder="Vehicle Model" required />

            <select id="vehicle-type" name="type" class="custom-input">
                <option value="none" selected disabled hidden>
                    Select Vehicle Type
                </option>
                <option value="cycle">Cycle</option>
                <option value="motor-cycle">Two Wheeler</option>
                <option value="four-wheeler">Four Wheeler</option>
            </select>

            <input name="vehicle number" class="custom-input" type="text" placeholder="Vehicle Number" required />
            <input name="employee id" class="custom-input" type="text" placeholder="Employee Id" id="employee-id"
                required />
            <textarea name="vehicle identification" class="custom-input"
                placeholder="Identification (Color, Mark, Visible Accessory etc.):"></textarea>
            <button class="form-button" type="button">SUBMIT</button>
        </form>
        <img class="form-image" src="images/meta-vehicle.png" alt="Vehicle image">
        `;

        this.pass = new Pass();
    }

    initializeVehicle() {
        let vehicleDiv = document.getElementById("vehicle-collapsible");
        vehicleDiv.innerHTML = this.template;
        this.vehicleFunction();
    }

    initializeForm() {
        this.formFields = document.querySelectorAll(`#vehicle-form > .custom-input`); // Get all input fields
        for (let i = 1; i < this.formFields.length; i++) {
            this.formFields[i].style.display = "none"; // Set display none to hide all inputs
        }
        this.formBtn = document.querySelector(`#vehicle-form button`);
        this.formBtn.style.display = "none";
    }

    vehicleFunction() {
        const header_span = document.getElementById("vehicle-input-heading");
        this.initializeForm();
        document.getElementById('menu-checkbox-vehicle').click();

        for (let i = 0; i < this.formFields.length - 1; i++) {
            let element = this.formFields[i] as HTMLInputElement;

            let parent = this;

            element.addEventListener("keypress", function (event) {
                if (element.value.length != 0 &&
                    event.keyCode == 13 && element.value != 'none') { // If enter is pressed and data is entered
                    let nextElement = element.nextElementSibling as HTMLInputElement;
                    let msg = `Can we know your ${nextElement.name}?`;
                    console.log(msg);

                    if (element.name == "type") {
                        parent.vehicleType = element.value;
                    }

                    header_span.innerHTML = `${msg}`;
                    nextElement.style.display = "block"; // Show next element and focus
                    element.style.display = "none";
                    nextElement.focus();

                    if (element.id == "vehicle-type") {
                        parent.vehicleType = element.value;
                    }

                    if (i == parent.formFields.length - 2) {
                        parent.formBtn.style.display = "block";
                        parent.formBtn.addEventListener("click", function () {

                            toggleAlert("block", "Thanks for telling us about your vehicle", "alert-success");

                            document.getElementById('menu-checkbox-vehicle').click();
                            header_span.innerHTML = "Thanks for telling us about your vehicle";
                            Vehicle.flagVehicleForm = true;

                            parent.formBtn.style.display = "none";
                            nextElement.style.display = "none";
                            setTimeout(function () {
                                toggleAlert("none", null, null);
                                parent.pass.initializePass(parent.vehicleType);
                            }, 3000);
                        });
                    }
                }
            });
        }
    }

    static isVehicleFormFilled() {
        return Vehicle.flagVehicleForm;
    }
}