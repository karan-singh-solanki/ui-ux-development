import { Employee } from "./Employee";
import { Vehicle } from "./Vehicle";

const alertDiv = document.getElementById("alert-div");
alertDiv.style.display = "none";

toggleAlert("none", null, "alert-warning");

let employee = new Employee();
employee.initializeEmployee();

document.getElementById("vehicle-collapse-toggle").addEventListener("click", function () {
    if (!employee.isEmployeeFormFilled()) {
        document.getElementById('menu-checkbox-vehicle').click();
        toggleAlert("block", "Please fill the Employee form first!", "alert-warning");
        setTimeout(function () {
            toggleAlert("none", null, null);
        }, 3000);
    }
});

document.getElementById("pricing-collapse-toggle").addEventListener("click", function () {
    if (!Vehicle.isVehicleFormFilled()) {
        document.getElementById('menu-checkbox-pricing').click();
        toggleAlert("block", "Please fill the Vehicle form first!", "alert-warning");
        setTimeout(function () {
            toggleAlert("none", null, null);
        }, 3000);
    }
});

export function toggleAlert(display: string, msg: string, classType: string) {
    alertDiv.removeAttribute("class");
    alertDiv.classList.add("alert");
    alertDiv.classList.add(classType);

    alertDiv.innerHTML = msg;
    alertDiv.style.display = display;
}
