(function(){function r(e,n,t){function o(i,f){if(!n[i]){if(!e[i]){var c="function"==typeof require&&require;if(!f&&c)return c(i,!0);if(u)return u(i,!0);var a=new Error("Cannot find module '"+i+"'");throw a.code="MODULE_NOT_FOUND",a}var p=n[i]={exports:{}};e[i][0].call(p.exports,function(r){var n=e[i][1][r];return o(n||r)},p,p.exports,r,e,n,t)}return n[i].exports}for(var u="function"==typeof require&&require,i=0;i<t.length;i++)o(t[i]);return o}return r})()({1:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Employee = void 0;
const Vehicle_1 = require("./Vehicle");
const app_1 = require("./app");
class Employee {
    constructor() {
        this.password = 'none';
        this.strongRegex = new RegExp("^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{10,})");
        this.mediumRegex = new RegExp("^(((?=.*[a-z])(?=.*[A-Z]))|((?=.*[a-z])(?=.*[0-9]))|((?=.*[A-Z])(?=.*[0-9])))(?=.{7,})");
        this.enoughRegex = new RegExp("(?=.{0,}).*");
        this.flagEmployeeForm = false;
        this.template = `
            <img class="form-image" src="images/meta-employee.png" alt="Employee image">
            <form id="employee-form">
                <span id="employee-input-heading">Welcome, Can we know your name?</span>
                <input class="custom-input" type="text" placeholder="Full Name" />
                <select id="select-gender" name="gender" class="custom-input">
                    <option value="none" selected disabled hidden>
                        Select Gender
                    </option>
                    <option value="Male">Male</option>
                    <option value="Female">Female</option>
                </select>
                <input name="email" class="custom-input" type="email" placeholder="Email" />
                <input id="password" name="password" class="custom-input" type="password" placeholder="Password" />
                <input name="confirm-password" class="custom-input" type="password" placeholder="Confirm password" />
                <input id="contact" name="contact" class="custom-input" type="number" placeholder="Contact"
                    pattern=".{8,}" title="Eight or more characters" required>
                <button class="form-button" type="button">SUBMIT</button>
            </form>
        `;
        this.vehicle = new Vehicle_1.Vehicle();
    }
    validate(inputType, value) {
        switch (inputType) {
            case "text":
                if (value.length < 2 || this.isNumeric(value)) {
                    alert(`Enter valid Name`);
                    return false;
                }
                break;
            case "password":
                console.log("pas: " + value);
                if (this.password != 'none') {
                    if (this.password == value) {
                        console.log("C pas: " + value);
                        return true;
                    }
                    else {
                        return false;
                    }
                }
                if (value.length < 8 || value.search(/[0-9]/) < 0 || value.search(/[a-z]/i) < 0 || value.search(/[A-Z]/i) < 0) {
                    alert(`should contains Uppercase, Lowercase, Numeric, Alphanumeric, and length minimum 8`);
                    this.password = 'none';
                    return false;
                }
                break;
            case "email":
                var atposition = value.indexOf("@");
                var dotposition = value.lastIndexOf(".");
                if (atposition < 1 || dotposition < atposition + 2 || dotposition + 2 >= value.length) {
                    return false;
                }
                break;
            case "number":
                console.log(value + "" + !this.isNumeric(value) || value.length < 8);
                if (!this.isNumeric(value) || value.length < 8) {
                    alert("Please enter a valid contact number");
                    return false;
                }
            case "gender":
                if (value == 'none') {
                    console.log(value + "gender");
                    return false;
                }
        }
        return true;
    }
    isNumeric(value) {
        return /^-?\d+$/.test(value);
    }
    passwordChanged() {
        var pwd = document.getElementById("password");
        if (this.strongRegex.test(pwd.value)) {
            pwd.style.backgroundColor = 'green';
        }
        else if (this.mediumRegex.test(pwd.value)) {
            pwd.style.backgroundColor = 'orange';
        }
        else if (this.enoughRegex.test(pwd.value)) {
            pwd.style.backgroundColor = 'red';
        }
    }
    initializeEmployee() {
        let employeeDiv = document.getElementById("employee-collapsible");
        employeeDiv.innerHTML = this.template;
        this.employeeFunction();
    }
    initializeForm() {
        this.formFields = document.querySelectorAll(`#employee-form > .custom-input`); // Get all input fields
        for (let i = 1; i < this.formFields.length; i++) {
            this.formFields[i].style.display = "none"; // Set display none to hide all inputs
        }
        this.formBtn = document.querySelector(`#employee-form button`);
        this.formBtn.style.display = "none";
    }
    employeeFunction() {
        const header_span = document.getElementById("employee-input-heading");
        this.initializeForm();
        let name;
        for (let i = 0; i < this.formFields.length - 1; i++) {
            let element = this.formFields[i];
            let parent = this;
            element.addEventListener("keypress", function (event) {
                if (element.name == "password") {
                    parent.passwordChanged();
                }
                if (element.value.length != 0 &&
                    event.keyCode == 13 && parent.validate(element.type, element.value) && element.value != 'none') {
                    // If enter is pressed and data is entered
                    let nextElement = element.nextElementSibling;
                    let msg = `Can we know your ${nextElement.name}?`;
                    console.log(msg);
                    if (element.name == "password") {
                        parent.password = element.value;
                    }
                    if (!name) {
                        name = element.value; // Show Hello + <name>
                        msg = `Hi ${name}, ${msg}`;
                    }
                    header_span.innerHTML = `${msg}`;
                    nextElement.style.display = "block"; // Show next element and focus
                    element.style.display = "none";
                    nextElement.focus();
                    if (i == parent.formFields.length - 2) {
                        parent.formBtn.style.display = "block";
                        parent.formBtn.addEventListener("click", function () {
                            if (!parent.validate(nextElement.type, nextElement.value))
                                return false;
                            (0, app_1.toggleAlert)("block", "Thanks for telling us about yourself, Your ID is: 1", "alert-success");
                            document.getElementById('menu-checkbox-employee').click();
                            header_span.innerHTML = "Thanks for telling us about yourself, Your ID is: 1";
                            parent.flagEmployeeForm = true;
                            parent.formBtn.style.display = "none";
                            nextElement.style.display = "none";
                            setTimeout(function () {
                                (0, app_1.toggleAlert)("none", null, null);
                                parent.vehicle.initializeVehicle();
                            }, 3000);
                        });
                    }
                }
            });
        }
    }
    isEmployeeFormFilled() {
        return this.flagEmployeeForm;
    }
}
exports.Employee = Employee;
},{"./Vehicle":3,"./app":4}],2:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Pass = void 0;
class Pass {
    constructor() {
        this.flagPricing = false;
        this.currency = "dollar";
        this.template = `<!--Cycle-->
        <!-- <span id="cycle-span-final-price" class="final-price"></span> -->
        <div class="vehicle-type" id="cycle-div">
            <h3>CYCLE</h3>
            <div class="circle center-flex">
                <label id="cycle-price" class="label-month-price">$100</label>
                <label id="cycle-duration">/month</label>
            </div>
            <select id="cycle-currency" class="custom-input change-curr">
                <option value="dollar" selected>$ Dollar</option>
                <option value="yen">&#165; YEN</option>
            </select>
            <div class="labels-price center-flex">
                <select id="cycle" class="custom-input purchase-curr">
                    <option value="none" selected disabled hidden>
                        Select Plan
                    </option>
                    <option data-price="5" , data-duration="daily">Daily</option>
                    <option data-price="100" , data-duration="monthly">Monthly</option>
                    <option data-price="500" , data-duration="yearly">Yearly</option>
                </select>
            </div>
            <button id="cycle-button" type="button">GET PASS</button>
        </div>

        <!--Motor Cycle-->
        <!-- <span id="motor-cycle-span-final-price" class="final-price"></span> -->
        <div class="vehicle-type" id="motor-cycle-div">
            <h3>MOTOR CYCLE</h3>
            <div class="circle center-flex">
                <label id="motor-cycle-price" class="label-month-price">$200</label>
                <label id="motor-cycle-duration">/month</label>
            </div>
            <div class="labels-price center-flex">
                <select id="motor-cycle-currency" class="custom-input change-curr">
                    <option value="dollar" selected>$ Dollar</option>
                    <option value="yen">¥ YEN</option>
                </select>

                <select id="motor-cycle" class="custom-input purchase-curr">
                    <option value="none" selected disabled hidden>
                        Select Plan
                    </option>
                    <option data-price="10" , data-duration="daily">Daily</option>
                    <option data-price="200" , data-duration="monthly">Monthly</option>
                    <option data-price="1000" , data-duration="yearly">Yearly</option>
                </select>
            </div>
            <button id="motor-cycle-button" type="button">GET PASS</button>
        </div>

        <!--Four Wheeler-->
        <!-- <span id="four-wheeler-final-price" class="final-price"></span> -->
        <div class="vehicle-type" id="four-wheeler-div">
            <h3>FOUR WHEELER</h3>
            <div class="circle center-flex">
                <label id="four-wheeler-price" class="label-month-price">$500</label>
                <label id="four-wheeler-duration">/month</label>
            </div>
            <select id="four-wheeler-currency" class="custom-input change-curr">
                <option value="dollar" selected>$ Dollar</option>
                <option value="yen">&#165; YEN</option>
            </select>
            <div class="labels-price center-flex">
                <select id="four-wheeler" class="custom-input purchase-curr">
                    <option value="none" selected disabled hidden>
                        Select Plan
                    </option>
                    <option data-price="20" , data-duration="daily">Daily</option>
                    <option data-price="400" , data-duration="monthly">Monthly</option>
                    <option data-price="2000" , data-duration="yearly">Yearly</option>
                </select>
            </div>

            <button id="four-wheeler-button" type="button">GET PASS</button>
        </div>`;
    }
    initializePass(vType) {
        this.vehicleType = vType;
        let vehicleDiv = document.getElementById("pricing-list");
        vehicleDiv.innerHTML = this.template;
        let parent = this;
        let changeCurr = function () {
            if (this.currency == ("dollar"))
                this.currency = "yen";
            else
                this.currency = "dollar";
        };
        document.querySelectorAll('.change-curr').forEach(item => {
            item.addEventListener('change', changeCurr);
        });
        document.querySelectorAll('.purchase-curr').forEach(item => {
            item.addEventListener("change", _ => {
                var id = item.id;
                var currencySymbol = parent.currency == "dollar" ? "$" : '¥';
                var selectedIndex = document.getElementById(id).selectedIndex;
                var selectedOption = document.getElementById(id).options[selectedIndex];
                var price = parseInt(selectedOption.dataset.price);
                if (currencySymbol != "$")
                    price = price / 2;
                document.getElementById(`${id}-price`).textContent = `${currencySymbol}${price}`;
                document.getElementById(`${id}-duration`).textContent = `/${selectedOption.dataset.duration}`;
                var btn = document.getElementById(`${id}-button`);
                btn.style.display = "block";
                btn.onclick = function () {
                    document.getElementById(`${id}-div`).style.display = 'none';
                    document.getElementById("pricing-list").innerHTML = `<h2> Final Price:  $ ${selectedOption.dataset.price} </h2>`;
                };
            });
        });
        this.pricingFunction();
    }
    pricingFunction() {
        document.getElementById('menu-checkbox-pricing').click();
        var vehicles = document.querySelectorAll(`.vehicle-type`);
        for (let i = 0; i < vehicles.length; i++) {
            if (vehicles[i] != document.getElementById(`${this.vehicleType}-div`))
                vehicles[i].style.display = "none"; // Set display none to hide all inputs
        }
        document.getElementById(`${this.vehicleType}-button`).style.display = "none";
    }
}
exports.Pass = Pass;
},{}],3:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Vehicle = void 0;
const Pass_1 = require("./Pass");
const app_1 = require("./app");
class Vehicle {
    constructor() {
        this.template = `
            <form id="vehicle-form">
            <span id="vehicle-input-heading">Can we know about your vehicle?</span>
            <input name="vehicle make" class="custom-input" type="text" placeholder="Vehicle Make (Company)"
                required />
            <input name="vehicle model" class="custom-input" type="text" placeholder="Vehicle Model" required />

            <select id="vehicle-type" name="type" class="custom-input">
                <option value="none" selected disabled hidden>
                    Select Vehicle Type
                </option>
                <option value="cycle">Cycle</option>
                <option value="motor-cycle">Two Wheeler</option>
                <option value="four-wheeler">Four Wheeler</option>
            </select>

            <input name="vehicle number" class="custom-input" type="text" placeholder="Vehicle Number" required />
            <input name="employee id" class="custom-input" type="text" placeholder="Employee Id" id="employee-id"
                required />
            <textarea name="vehicle identification" class="custom-input"
                placeholder="Identification (Color, Mark, Visible Accessory etc.):"></textarea>
            <button class="form-button" type="button">SUBMIT</button>
        </form>
        <img class="form-image" src="images/meta-vehicle.png" alt="Vehicle image">
        `;
        this.pass = new Pass_1.Pass();
    }
    initializeVehicle() {
        let vehicleDiv = document.getElementById("vehicle-collapsible");
        vehicleDiv.innerHTML = this.template;
        this.vehicleFunction();
    }
    initializeForm() {
        this.formFields = document.querySelectorAll(`#vehicle-form > .custom-input`); // Get all input fields
        for (let i = 1; i < this.formFields.length; i++) {
            this.formFields[i].style.display = "none"; // Set display none to hide all inputs
        }
        this.formBtn = document.querySelector(`#vehicle-form button`);
        this.formBtn.style.display = "none";
    }
    vehicleFunction() {
        const header_span = document.getElementById("vehicle-input-heading");
        this.initializeForm();
        document.getElementById('menu-checkbox-vehicle').click();
        for (let i = 0; i < this.formFields.length - 1; i++) {
            let element = this.formFields[i];
            let parent = this;
            element.addEventListener("keypress", function (event) {
                if (element.value.length != 0 &&
                    event.keyCode == 13 && element.value != 'none') { // If enter is pressed and data is entered
                    let nextElement = element.nextElementSibling;
                    let msg = `Can we know your ${nextElement.name}?`;
                    console.log(msg);
                    if (element.name == "type") {
                        parent.vehicleType = element.value;
                    }
                    header_span.innerHTML = `${msg}`;
                    nextElement.style.display = "block"; // Show next element and focus
                    element.style.display = "none";
                    nextElement.focus();
                    if (element.id == "vehicle-type") {
                        parent.vehicleType = element.value;
                    }
                    if (i == parent.formFields.length - 2) {
                        parent.formBtn.style.display = "block";
                        parent.formBtn.addEventListener("click", function () {
                            (0, app_1.toggleAlert)("block", "Thanks for telling us about your vehicle", "alert-success");
                            document.getElementById('menu-checkbox-vehicle').click();
                            header_span.innerHTML = "Thanks for telling us about your vehicle";
                            Vehicle.flagVehicleForm = true;
                            parent.formBtn.style.display = "none";
                            nextElement.style.display = "none";
                            setTimeout(function () {
                                (0, app_1.toggleAlert)("none", null, null);
                                parent.pass.initializePass(parent.vehicleType);
                            }, 3000);
                        });
                    }
                }
            });
        }
    }
    static isVehicleFormFilled() {
        return Vehicle.flagVehicleForm;
    }
}
exports.Vehicle = Vehicle;
Vehicle.flagVehicleForm = false;
},{"./Pass":2,"./app":4}],4:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.toggleAlert = void 0;
const Employee_1 = require("./Employee");
const Vehicle_1 = require("./Vehicle");
const alertDiv = document.getElementById("alert-div");
alertDiv.style.display = "none";
toggleAlert("none", null, "alert-warning");
let employee = new Employee_1.Employee();
employee.initializeEmployee();
document.getElementById("vehicle-collapse-toggle").addEventListener("click", function () {
    if (!employee.isEmployeeFormFilled()) {
        document.getElementById('menu-checkbox-vehicle').click();
        toggleAlert("block", "Please fill the Employee form first!", "alert-warning");
        setTimeout(function () {
            toggleAlert("none", null, null);
        }, 3000);
    }
});
document.getElementById("pricing-collapse-toggle").addEventListener("click", function () {
    if (!Vehicle_1.Vehicle.isVehicleFormFilled()) {
        document.getElementById('menu-checkbox-pricing').click();
        toggleAlert("block", "Please fill the Vehicle form first!", "alert-warning");
        setTimeout(function () {
            toggleAlert("none", null, null);
        }, 3000);
    }
});
function toggleAlert(display, msg, classType) {
    alertDiv.removeAttribute("class");
    alertDiv.classList.add("alert");
    alertDiv.classList.add(classType);
    alertDiv.innerHTML = msg;
    alertDiv.style.display = display;
}
exports.toggleAlert = toggleAlert;
},{"./Employee":1,"./Vehicle":3}]},{},[4])
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm5vZGVfbW9kdWxlcy9icm93c2VyLXBhY2svX3ByZWx1ZGUuanMiLCJzcmMvc2NyaXB0cy9FbXBsb3llZS50cyIsInNyYy9zY3JpcHRzL1Bhc3MudHMiLCJzcmMvc2NyaXB0cy9WZWhpY2xlLnRzIiwic3JjL3NjcmlwdHMvYXBwLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBOzs7O0FDQUEsdUNBQW9DO0FBQ3BDLCtCQUFvQztBQUVwQyxNQUFhLFFBQVE7SUFpQmpCO1FBaEJBLGFBQVEsR0FBRyxNQUFNLENBQUM7UUFDbEIsZ0JBQVcsR0FBRyxJQUFJLE1BQU0sQ0FBQyxpRUFBaUUsQ0FBQyxDQUFDO1FBQzVGLGdCQUFXLEdBQUcsSUFBSSxNQUFNLENBQUMsd0ZBQXdGLENBQUMsQ0FBQztRQUNuSCxnQkFBVyxHQUFHLElBQUksTUFBTSxDQUFDLGFBQWEsQ0FBQyxDQUFDO1FBV3hDLHFCQUFnQixHQUFHLEtBQUssQ0FBQztRQUdyQixJQUFJLENBQUMsUUFBUSxHQUFHOzs7Ozs7Ozs7Ozs7Ozs7Ozs7O1NBbUJmLENBQUM7UUFDRixJQUFJLENBQUMsT0FBTyxHQUFHLElBQUksaUJBQU8sRUFBRSxDQUFDO0lBQ2pDLENBQUM7SUFFRCxRQUFRLENBQUMsU0FBaUIsRUFBRSxLQUFhO1FBQ3JDLFFBQVEsU0FBUyxFQUFFO1lBQ2YsS0FBSyxNQUFNO2dCQUNQLElBQUksS0FBSyxDQUFDLE1BQU0sR0FBRyxDQUFDLElBQUksSUFBSSxDQUFDLFNBQVMsQ0FBQyxLQUFLLENBQUMsRUFBRTtvQkFDM0MsS0FBSyxDQUFDLGtCQUFrQixDQUFDLENBQUM7b0JBQzFCLE9BQU8sS0FBSyxDQUFDO2lCQUNoQjtnQkFDRCxNQUFNO1lBRVYsS0FBSyxVQUFVO2dCQUNYLE9BQU8sQ0FBQyxHQUFHLENBQUMsT0FBTyxHQUFHLEtBQUssQ0FBQyxDQUFBO2dCQUM1QixJQUFJLElBQUksQ0FBQyxRQUFRLElBQUksTUFBTSxFQUFFO29CQUV6QixJQUFJLElBQUksQ0FBQyxRQUFRLElBQUksS0FBSyxFQUFFO3dCQUN4QixPQUFPLENBQUMsR0FBRyxDQUFDLFNBQVMsR0FBRyxLQUFLLENBQUMsQ0FBQTt3QkFDOUIsT0FBTyxJQUFJLENBQUM7cUJBQ2Y7eUJBQU07d0JBQ0gsT0FBTyxLQUFLLENBQUM7cUJBQ2hCO2lCQUNKO2dCQUVELElBQUksS0FBSyxDQUFDLE1BQU0sR0FBRyxDQUFDLElBQUksS0FBSyxDQUFDLE1BQU0sQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLElBQUksS0FBSyxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsR0FBRyxDQUFDLElBQUksS0FBSyxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsR0FBRyxDQUFDLEVBQUU7b0JBQzNHLEtBQUssQ0FBQyxtRkFBbUYsQ0FBQyxDQUFDO29CQUMzRixJQUFJLENBQUMsUUFBUSxHQUFHLE1BQU0sQ0FBQztvQkFDdkIsT0FBTyxLQUFLLENBQUM7aUJBQ2hCO2dCQUNELE1BQU07WUFFVixLQUFLLE9BQU87Z0JBQ1IsSUFBSSxVQUFVLEdBQUcsS0FBSyxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsQ0FBQztnQkFDcEMsSUFBSSxXQUFXLEdBQUcsS0FBSyxDQUFDLFdBQVcsQ0FBQyxHQUFHLENBQUMsQ0FBQztnQkFDekMsSUFBSSxVQUFVLEdBQUcsQ0FBQyxJQUFJLFdBQVcsR0FBRyxVQUFVLEdBQUcsQ0FBQyxJQUFJLFdBQVcsR0FBRyxDQUFDLElBQUksS0FBSyxDQUFDLE1BQU0sRUFBRTtvQkFDbkYsT0FBTyxLQUFLLENBQUM7aUJBQ2hCO2dCQUNELE1BQU07WUFFVixLQUFLLFFBQVE7Z0JBQ1QsT0FBTyxDQUFDLEdBQUcsQ0FBQyxLQUFLLEdBQUcsRUFBRSxHQUFHLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxLQUFLLENBQUMsSUFBSSxLQUFLLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQyxDQUFDO2dCQUNyRSxJQUFJLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxLQUFLLENBQUMsSUFBSSxLQUFLLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRTtvQkFDNUMsS0FBSyxDQUFDLHFDQUFxQyxDQUFDLENBQUM7b0JBQzdDLE9BQU8sS0FBSyxDQUFDO2lCQUNoQjtZQUVMLEtBQUssUUFBUTtnQkFDVCxJQUFJLEtBQUssSUFBSSxNQUFNLEVBQUU7b0JBQ2pCLE9BQU8sQ0FBQyxHQUFHLENBQUMsS0FBSyxHQUFHLFFBQVEsQ0FBQyxDQUFDO29CQUM5QixPQUFPLEtBQUssQ0FBQztpQkFDaEI7U0FDUjtRQUNELE9BQU8sSUFBSSxDQUFDO0lBQ2hCLENBQUM7SUFFRCxTQUFTLENBQUMsS0FBYTtRQUNuQixPQUFPLFNBQVMsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7SUFDakMsQ0FBQztJQUVELGVBQWU7UUFDWCxJQUFJLEdBQUcsR0FBRyxRQUFRLENBQUMsY0FBYyxDQUFDLFVBQVUsQ0FBcUIsQ0FBQztRQUVsRSxJQUFJLElBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsRUFBRTtZQUNsQyxHQUFHLENBQUMsS0FBSyxDQUFDLGVBQWUsR0FBRyxPQUFPLENBQUM7U0FDdkM7YUFBTSxJQUFJLElBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsRUFBRTtZQUN6QyxHQUFHLENBQUMsS0FBSyxDQUFDLGVBQWUsR0FBRyxRQUFRLENBQUM7U0FDeEM7YUFBTSxJQUFJLElBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsRUFBRTtZQUN6QyxHQUFHLENBQUMsS0FBSyxDQUFDLGVBQWUsR0FBRyxLQUFLLENBQUM7U0FDckM7SUFDTCxDQUFDO0lBRUQsa0JBQWtCO1FBQ2QsSUFBSSxXQUFXLEdBQUcsUUFBUSxDQUFDLGNBQWMsQ0FBQyxzQkFBc0IsQ0FBQyxDQUFBO1FBQ2pFLFdBQVcsQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQztRQUN0QyxJQUFJLENBQUMsZ0JBQWdCLEVBQUUsQ0FBQztJQUM1QixDQUFDO0lBRUQsY0FBYztRQUNWLElBQUksQ0FBQyxVQUFVLEdBQUcsUUFBUSxDQUFDLGdCQUFnQixDQUFDLGdDQUFnQyxDQUFDLENBQUMsQ0FBQyx1QkFBdUI7UUFDdEcsS0FBSyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLElBQUksQ0FBQyxVQUFVLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSxFQUFFO1lBQzdDLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLE9BQU8sR0FBRyxNQUFNLENBQUMsQ0FBQyxzQ0FBc0M7U0FDcEY7UUFDRCxJQUFJLENBQUMsT0FBTyxHQUFHLFFBQVEsQ0FBQyxhQUFhLENBQUMsdUJBQXVCLENBQUMsQ0FBQztRQUMvRCxJQUFJLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxPQUFPLEdBQUcsTUFBTSxDQUFDO0lBQ3hDLENBQUM7SUFFRCxnQkFBZ0I7UUFDWixNQUFNLFdBQVcsR0FBRyxRQUFRLENBQUMsY0FBYyxDQUFDLHdCQUF3QixDQUFDLENBQUM7UUFDdEUsSUFBSSxDQUFDLGNBQWMsRUFBRSxDQUFDO1FBRXRCLElBQUksSUFBWSxDQUFDO1FBRWpCLEtBQUssSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUUsQ0FBQyxFQUFFLEVBQUU7WUFDakQsSUFBSSxPQUFPLEdBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQXFCLENBQUM7WUFFckQsSUFBSSxNQUFNLEdBQUcsSUFBSSxDQUFDO1lBRWxCLE9BQU8sQ0FBQyxnQkFBZ0IsQ0FBQyxVQUFVLEVBQUUsVUFBVSxLQUFLO2dCQUNoRCxJQUFJLE9BQU8sQ0FBQyxJQUFJLElBQUksVUFBVSxFQUFFO29CQUM1QixNQUFNLENBQUMsZUFBZSxFQUFFLENBQUM7aUJBQzVCO2dCQUNELElBQUksT0FBTyxDQUFDLEtBQUssQ0FBQyxNQUFNLElBQUksQ0FBQztvQkFDekIsS0FBSyxDQUFDLE9BQU8sSUFBSSxFQUFFLElBQUksTUFBTSxDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUMsSUFBSSxFQUFFLE9BQU8sQ0FBQyxLQUFLLENBQUMsSUFBSSxPQUFPLENBQUMsS0FBSyxJQUFJLE1BQU0sRUFBRTtvQkFDaEcsMENBQTBDO29CQUMxQyxJQUFJLFdBQVcsR0FBRyxPQUFPLENBQUMsa0JBQXNDLENBQUM7b0JBQ2pFLElBQUksR0FBRyxHQUFHLG9CQUFvQixXQUFXLENBQUMsSUFBSSxHQUFHLENBQUM7b0JBQ2xELE9BQU8sQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDLENBQUM7b0JBRWpCLElBQUksT0FBTyxDQUFDLElBQUksSUFBSSxVQUFVLEVBQUU7d0JBQzVCLE1BQU0sQ0FBQyxRQUFRLEdBQUcsT0FBTyxDQUFDLEtBQUssQ0FBQztxQkFDbkM7b0JBRUQsSUFBSSxDQUFDLElBQUksRUFBRTt3QkFDUCxJQUFJLEdBQUcsT0FBTyxDQUFDLEtBQUssQ0FBQyxDQUFDLHNCQUFzQjt3QkFDNUMsR0FBRyxHQUFHLE1BQU0sSUFBSSxLQUFLLEdBQUcsRUFBRSxDQUFDO3FCQUM5QjtvQkFFRCxXQUFXLENBQUMsU0FBUyxHQUFHLEdBQUcsR0FBRyxFQUFFLENBQUM7b0JBQ2pDLFdBQVcsQ0FBQyxLQUFLLENBQUMsT0FBTyxHQUFHLE9BQU8sQ0FBQyxDQUFDLDhCQUE4QjtvQkFDbkUsT0FBTyxDQUFDLEtBQUssQ0FBQyxPQUFPLEdBQUcsTUFBTSxDQUFDO29CQUMvQixXQUFXLENBQUMsS0FBSyxFQUFFLENBQUM7b0JBRXBCLElBQUksQ0FBQyxJQUFJLE1BQU0sQ0FBQyxVQUFVLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRTt3QkFDbkMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsT0FBTyxHQUFHLE9BQU8sQ0FBQzt3QkFFdkMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxnQkFBZ0IsQ0FBQyxPQUFPLEVBQUU7NEJBQ3JDLElBQUksQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxJQUFJLEVBQUUsV0FBVyxDQUFDLEtBQUssQ0FBQztnQ0FDckQsT0FBTyxLQUFLLENBQUM7NEJBRWpCLElBQUEsaUJBQVcsRUFBQyxPQUFPLEVBQUUscURBQXFELEVBQUUsZUFBZSxDQUFDLENBQUM7NEJBRTdGLFFBQVEsQ0FBQyxjQUFjLENBQUMsd0JBQXdCLENBQUMsQ0FBQyxLQUFLLEVBQUUsQ0FBQzs0QkFDMUQsV0FBVyxDQUFDLFNBQVMsR0FBRyxxREFBcUQsQ0FBQzs0QkFDOUUsTUFBTSxDQUFDLGdCQUFnQixHQUFHLElBQUksQ0FBQzs0QkFFL0IsTUFBTSxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsT0FBTyxHQUFHLE1BQU0sQ0FBQzs0QkFDdEMsV0FBVyxDQUFDLEtBQUssQ0FBQyxPQUFPLEdBQUcsTUFBTSxDQUFDOzRCQUNuQyxVQUFVLENBQUM7Z0NBQ1AsSUFBQSxpQkFBVyxFQUFDLE1BQU0sRUFBRSxJQUFJLEVBQUUsSUFBSSxDQUFDLENBQUM7Z0NBQ2hDLE1BQU0sQ0FBQyxPQUFPLENBQUMsaUJBQWlCLEVBQUUsQ0FBQzs0QkFDdkMsQ0FBQyxFQUFFLElBQUksQ0FBQyxDQUFDO3dCQUNiLENBQUMsQ0FBQyxDQUFDO3FCQUNOO2lCQUVKO1lBQ0wsQ0FBQyxDQUFDLENBQUM7U0FDTjtJQUNMLENBQUM7SUFFRCxvQkFBb0I7UUFDaEIsT0FBTyxJQUFJLENBQUMsZ0JBQWdCLENBQUM7SUFDakMsQ0FBQztDQUNKO0FBOUxELDRCQThMQzs7Ozs7QUNqTUQsTUFBYSxJQUFJO0lBT2I7UUFDSSxJQUFJLENBQUMsV0FBVyxHQUFHLEtBQUssQ0FBQztRQUN6QixJQUFJLENBQUMsUUFBUSxHQUFHLFFBQVEsQ0FBQztRQUN6QixJQUFJLENBQUMsUUFBUSxHQUFHOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7ZUEyRVQsQ0FBQztJQUNaLENBQUM7SUFFRCxjQUFjLENBQUMsS0FBYTtRQUN4QixJQUFJLENBQUMsV0FBVyxHQUFHLEtBQUssQ0FBQztRQUN6QixJQUFJLFVBQVUsR0FBRyxRQUFRLENBQUMsY0FBYyxDQUFDLGNBQWMsQ0FBQyxDQUFDO1FBQ3pELFVBQVUsQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQztRQUNyQyxJQUFJLE1BQU0sR0FBRyxJQUFJLENBQUM7UUFFbEIsSUFBSSxVQUFVLEdBQUc7WUFDYixJQUFJLElBQUksQ0FBQyxRQUFRLElBQUksQ0FBQyxRQUFRLENBQUM7Z0JBQzNCLElBQUksQ0FBQyxRQUFRLEdBQUcsS0FBSyxDQUFDOztnQkFFdEIsSUFBSSxDQUFDLFFBQVEsR0FBRyxRQUFRLENBQUM7UUFDakMsQ0FBQyxDQUFDO1FBRUYsUUFBUSxDQUFDLGdCQUFnQixDQUFDLGNBQWMsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsRUFBRTtZQUNyRCxJQUFJLENBQUMsZ0JBQWdCLENBQUMsUUFBUSxFQUFFLFVBQVUsQ0FBQyxDQUFBO1FBQy9DLENBQUMsQ0FBQyxDQUFBO1FBRUYsUUFBUSxDQUFDLGdCQUFnQixDQUFDLGdCQUFnQixDQUFDLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxFQUFFO1lBQ3ZELElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxRQUFRLEVBQUUsQ0FBQyxDQUFDLEVBQUU7Z0JBQ2hDLElBQUksRUFBRSxHQUFHLElBQUksQ0FBQyxFQUFFLENBQUM7Z0JBQ2pCLElBQUksY0FBYyxHQUFHLE1BQU0sQ0FBQyxRQUFRLElBQUksUUFBUSxDQUFDLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLEdBQUcsQ0FBQztnQkFFN0QsSUFBSSxhQUFhLEdBQUksUUFBUSxDQUFDLGNBQWMsQ0FBQyxFQUFFLENBQXVCLENBQUMsYUFBYSxDQUFDO2dCQUNyRixJQUFJLGNBQWMsR0FBSSxRQUFRLENBQUMsY0FBYyxDQUFDLEVBQUUsQ0FBdUIsQ0FBQyxPQUFPLENBQUMsYUFBYSxDQUFDLENBQUM7Z0JBRS9GLElBQUksS0FBSyxHQUFHLFFBQVEsQ0FBQyxjQUFjLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxDQUFDO2dCQUNuRCxJQUFJLGNBQWMsSUFBSSxHQUFHO29CQUNyQixLQUFLLEdBQUcsS0FBSyxHQUFHLENBQUMsQ0FBQztnQkFFdEIsUUFBUSxDQUFDLGNBQWMsQ0FBQyxHQUFHLEVBQUUsUUFBUSxDQUFDLENBQUMsV0FBVyxHQUFHLEdBQUcsY0FBYyxHQUFHLEtBQUssRUFBRSxDQUFDO2dCQUNqRixRQUFRLENBQUMsY0FBYyxDQUFDLEdBQUcsRUFBRSxXQUFXLENBQUMsQ0FBQyxXQUFXLEdBQUcsSUFBSSxjQUFjLENBQUMsT0FBTyxDQUFDLFFBQVEsRUFBRSxDQUFDO2dCQUU5RixJQUFJLEdBQUcsR0FBRyxRQUFRLENBQUMsY0FBYyxDQUFDLEdBQUcsRUFBRSxTQUFTLENBQUMsQ0FBQztnQkFDbEQsR0FBRyxDQUFDLEtBQUssQ0FBQyxPQUFPLEdBQUcsT0FBTyxDQUFDO2dCQUU1QixHQUFHLENBQUMsT0FBTyxHQUFHO29CQUNWLFFBQVEsQ0FBQyxjQUFjLENBQUMsR0FBRyxFQUFFLE1BQU0sQ0FBQyxDQUFDLEtBQUssQ0FBQyxPQUFPLEdBQUcsTUFBTSxDQUFDO29CQUM1RCxRQUFRLENBQUMsY0FBYyxDQUFDLGNBQWMsQ0FBQyxDQUFDLFNBQVMsR0FBRyx3QkFBd0IsY0FBYyxDQUFDLE9BQU8sQ0FBQyxLQUFLLFFBQVEsQ0FBQztnQkFDckgsQ0FBQyxDQUFDO1lBQ04sQ0FBQyxDQUFDLENBQUE7UUFDTixDQUFDLENBQUMsQ0FBQTtRQUNGLElBQUksQ0FBQyxlQUFlLEVBQUUsQ0FBQztJQUMzQixDQUFDO0lBR0QsZUFBZTtRQUNYLFFBQVEsQ0FBQyxjQUFjLENBQUMsdUJBQXVCLENBQUMsQ0FBQyxLQUFLLEVBQUUsQ0FBQztRQUV6RCxJQUFJLFFBQVEsR0FBRyxRQUFRLENBQUMsZ0JBQWdCLENBQUMsZUFBZSxDQUE0QixDQUFDO1FBQ3JGLEtBQUssSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxRQUFRLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSxFQUFFO1lBQ3RDLElBQUksUUFBUSxDQUFDLENBQUMsQ0FBQyxJQUFJLFFBQVEsQ0FBQyxjQUFjLENBQUMsR0FBRyxJQUFJLENBQUMsV0FBVyxNQUFNLENBQUM7Z0JBQ2pFLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsT0FBTyxHQUFHLE1BQU0sQ0FBQyxDQUFDLHNDQUFzQztTQUNqRjtRQUNBLFFBQVEsQ0FBQyxjQUFjLENBQUMsR0FBRyxJQUFJLENBQUMsV0FBVyxTQUFTLENBQXNCLENBQUMsS0FBSyxDQUFDLE9BQU8sR0FBRyxNQUFNLENBQUM7SUFDdkcsQ0FBQztDQUNKO0FBL0lELG9CQStJQzs7Ozs7QUMvSUQsaUNBQThCO0FBQzlCLCtCQUFvQztBQUVwQyxNQUFhLE9BQU87SUFVaEI7UUFDSSxJQUFJLENBQUMsUUFBUSxHQUFHOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7U0F3QmYsQ0FBQztRQUVGLElBQUksQ0FBQyxJQUFJLEdBQUcsSUFBSSxXQUFJLEVBQUUsQ0FBQztJQUMzQixDQUFDO0lBRUQsaUJBQWlCO1FBQ2IsSUFBSSxVQUFVLEdBQUcsUUFBUSxDQUFDLGNBQWMsQ0FBQyxxQkFBcUIsQ0FBQyxDQUFDO1FBQ2hFLFVBQVUsQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQztRQUNyQyxJQUFJLENBQUMsZUFBZSxFQUFFLENBQUM7SUFDM0IsQ0FBQztJQUVELGNBQWM7UUFDVixJQUFJLENBQUMsVUFBVSxHQUFHLFFBQVEsQ0FBQyxnQkFBZ0IsQ0FBQywrQkFBK0IsQ0FBQyxDQUFDLENBQUMsdUJBQXVCO1FBQ3JHLEtBQUssSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBRTtZQUM3QyxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxPQUFPLEdBQUcsTUFBTSxDQUFDLENBQUMsc0NBQXNDO1NBQ3BGO1FBQ0QsSUFBSSxDQUFDLE9BQU8sR0FBRyxRQUFRLENBQUMsYUFBYSxDQUFDLHNCQUFzQixDQUFDLENBQUM7UUFDOUQsSUFBSSxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsT0FBTyxHQUFHLE1BQU0sQ0FBQztJQUN4QyxDQUFDO0lBRUQsZUFBZTtRQUNYLE1BQU0sV0FBVyxHQUFHLFFBQVEsQ0FBQyxjQUFjLENBQUMsdUJBQXVCLENBQUMsQ0FBQztRQUNyRSxJQUFJLENBQUMsY0FBYyxFQUFFLENBQUM7UUFDdEIsUUFBUSxDQUFDLGNBQWMsQ0FBQyx1QkFBdUIsQ0FBQyxDQUFDLEtBQUssRUFBRSxDQUFDO1FBRXpELEtBQUssSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUUsQ0FBQyxFQUFFLEVBQUU7WUFDakQsSUFBSSxPQUFPLEdBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQXFCLENBQUM7WUFFckQsSUFBSSxNQUFNLEdBQUcsSUFBSSxDQUFDO1lBRWxCLE9BQU8sQ0FBQyxnQkFBZ0IsQ0FBQyxVQUFVLEVBQUUsVUFBVSxLQUFLO2dCQUNoRCxJQUFJLE9BQU8sQ0FBQyxLQUFLLENBQUMsTUFBTSxJQUFJLENBQUM7b0JBQ3pCLEtBQUssQ0FBQyxPQUFPLElBQUksRUFBRSxJQUFJLE9BQU8sQ0FBQyxLQUFLLElBQUksTUFBTSxFQUFFLEVBQUUsMENBQTBDO29CQUM1RixJQUFJLFdBQVcsR0FBRyxPQUFPLENBQUMsa0JBQXNDLENBQUM7b0JBQ2pFLElBQUksR0FBRyxHQUFHLG9CQUFvQixXQUFXLENBQUMsSUFBSSxHQUFHLENBQUM7b0JBQ2xELE9BQU8sQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDLENBQUM7b0JBRWpCLElBQUksT0FBTyxDQUFDLElBQUksSUFBSSxNQUFNLEVBQUU7d0JBQ3hCLE1BQU0sQ0FBQyxXQUFXLEdBQUcsT0FBTyxDQUFDLEtBQUssQ0FBQztxQkFDdEM7b0JBRUQsV0FBVyxDQUFDLFNBQVMsR0FBRyxHQUFHLEdBQUcsRUFBRSxDQUFDO29CQUNqQyxXQUFXLENBQUMsS0FBSyxDQUFDLE9BQU8sR0FBRyxPQUFPLENBQUMsQ0FBQyw4QkFBOEI7b0JBQ25FLE9BQU8sQ0FBQyxLQUFLLENBQUMsT0FBTyxHQUFHLE1BQU0sQ0FBQztvQkFDL0IsV0FBVyxDQUFDLEtBQUssRUFBRSxDQUFDO29CQUVwQixJQUFJLE9BQU8sQ0FBQyxFQUFFLElBQUksY0FBYyxFQUFFO3dCQUM5QixNQUFNLENBQUMsV0FBVyxHQUFHLE9BQU8sQ0FBQyxLQUFLLENBQUM7cUJBQ3RDO29CQUVELElBQUksQ0FBQyxJQUFJLE1BQU0sQ0FBQyxVQUFVLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRTt3QkFDbkMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsT0FBTyxHQUFHLE9BQU8sQ0FBQzt3QkFDdkMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxnQkFBZ0IsQ0FBQyxPQUFPLEVBQUU7NEJBRXJDLElBQUEsaUJBQVcsRUFBQyxPQUFPLEVBQUUsMENBQTBDLEVBQUUsZUFBZSxDQUFDLENBQUM7NEJBRWxGLFFBQVEsQ0FBQyxjQUFjLENBQUMsdUJBQXVCLENBQUMsQ0FBQyxLQUFLLEVBQUUsQ0FBQzs0QkFDekQsV0FBVyxDQUFDLFNBQVMsR0FBRywwQ0FBMEMsQ0FBQzs0QkFDbkUsT0FBTyxDQUFDLGVBQWUsR0FBRyxJQUFJLENBQUM7NEJBRS9CLE1BQU0sQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLE9BQU8sR0FBRyxNQUFNLENBQUM7NEJBQ3RDLFdBQVcsQ0FBQyxLQUFLLENBQUMsT0FBTyxHQUFHLE1BQU0sQ0FBQzs0QkFDbkMsVUFBVSxDQUFDO2dDQUNQLElBQUEsaUJBQVcsRUFBQyxNQUFNLEVBQUUsSUFBSSxFQUFFLElBQUksQ0FBQyxDQUFDO2dDQUNoQyxNQUFNLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxNQUFNLENBQUMsV0FBVyxDQUFDLENBQUM7NEJBQ25ELENBQUMsRUFBRSxJQUFJLENBQUMsQ0FBQzt3QkFDYixDQUFDLENBQUMsQ0FBQztxQkFDTjtpQkFDSjtZQUNMLENBQUMsQ0FBQyxDQUFDO1NBQ047SUFDTCxDQUFDO0lBRUQsTUFBTSxDQUFDLG1CQUFtQjtRQUN0QixPQUFPLE9BQU8sQ0FBQyxlQUFlLENBQUM7SUFDbkMsQ0FBQzs7QUE5R0wsMEJBK0dDO0FBdkdVLHVCQUFlLEdBQVksS0FBSyxDQUFDOzs7OztBQ1g1Qyx5Q0FBc0M7QUFDdEMsdUNBQW9DO0FBRXBDLE1BQU0sUUFBUSxHQUFHLFFBQVEsQ0FBQyxjQUFjLENBQUMsV0FBVyxDQUFDLENBQUM7QUFDdEQsUUFBUSxDQUFDLEtBQUssQ0FBQyxPQUFPLEdBQUcsTUFBTSxDQUFDO0FBRWhDLFdBQVcsQ0FBQyxNQUFNLEVBQUUsSUFBSSxFQUFFLGVBQWUsQ0FBQyxDQUFDO0FBRTNDLElBQUksUUFBUSxHQUFHLElBQUksbUJBQVEsRUFBRSxDQUFDO0FBQzlCLFFBQVEsQ0FBQyxrQkFBa0IsRUFBRSxDQUFDO0FBRTlCLFFBQVEsQ0FBQyxjQUFjLENBQUMseUJBQXlCLENBQUMsQ0FBQyxnQkFBZ0IsQ0FBQyxPQUFPLEVBQUU7SUFDekUsSUFBSSxDQUFDLFFBQVEsQ0FBQyxvQkFBb0IsRUFBRSxFQUFFO1FBQ2xDLFFBQVEsQ0FBQyxjQUFjLENBQUMsdUJBQXVCLENBQUMsQ0FBQyxLQUFLLEVBQUUsQ0FBQztRQUN6RCxXQUFXLENBQUMsT0FBTyxFQUFFLHNDQUFzQyxFQUFFLGVBQWUsQ0FBQyxDQUFDO1FBQzlFLFVBQVUsQ0FBQztZQUNQLFdBQVcsQ0FBQyxNQUFNLEVBQUUsSUFBSSxFQUFFLElBQUksQ0FBQyxDQUFDO1FBQ3BDLENBQUMsRUFBRSxJQUFJLENBQUMsQ0FBQztLQUNaO0FBQ0wsQ0FBQyxDQUFDLENBQUM7QUFFSCxRQUFRLENBQUMsY0FBYyxDQUFDLHlCQUF5QixDQUFDLENBQUMsZ0JBQWdCLENBQUMsT0FBTyxFQUFFO0lBQ3pFLElBQUksQ0FBQyxpQkFBTyxDQUFDLG1CQUFtQixFQUFFLEVBQUU7UUFDaEMsUUFBUSxDQUFDLGNBQWMsQ0FBQyx1QkFBdUIsQ0FBQyxDQUFDLEtBQUssRUFBRSxDQUFDO1FBQ3pELFdBQVcsQ0FBQyxPQUFPLEVBQUUscUNBQXFDLEVBQUUsZUFBZSxDQUFDLENBQUM7UUFDN0UsVUFBVSxDQUFDO1lBQ1AsV0FBVyxDQUFDLE1BQU0sRUFBRSxJQUFJLEVBQUUsSUFBSSxDQUFDLENBQUM7UUFDcEMsQ0FBQyxFQUFFLElBQUksQ0FBQyxDQUFDO0tBQ1o7QUFDTCxDQUFDLENBQUMsQ0FBQztBQUVILFNBQWdCLFdBQVcsQ0FBQyxPQUFlLEVBQUUsR0FBVyxFQUFFLFNBQWlCO0lBQ3ZFLFFBQVEsQ0FBQyxlQUFlLENBQUMsT0FBTyxDQUFDLENBQUM7SUFDbEMsUUFBUSxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQUMsT0FBTyxDQUFDLENBQUM7SUFDaEMsUUFBUSxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQUMsU0FBUyxDQUFDLENBQUM7SUFFbEMsUUFBUSxDQUFDLFNBQVMsR0FBRyxHQUFHLENBQUM7SUFDekIsUUFBUSxDQUFDLEtBQUssQ0FBQyxPQUFPLEdBQUcsT0FBTyxDQUFDO0FBQ3JDLENBQUM7QUFQRCxrQ0FPQyIsImZpbGUiOiJnZW5lcmF0ZWQuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlc0NvbnRlbnQiOlsiKGZ1bmN0aW9uKCl7ZnVuY3Rpb24gcihlLG4sdCl7ZnVuY3Rpb24gbyhpLGYpe2lmKCFuW2ldKXtpZighZVtpXSl7dmFyIGM9XCJmdW5jdGlvblwiPT10eXBlb2YgcmVxdWlyZSYmcmVxdWlyZTtpZighZiYmYylyZXR1cm4gYyhpLCEwKTtpZih1KXJldHVybiB1KGksITApO3ZhciBhPW5ldyBFcnJvcihcIkNhbm5vdCBmaW5kIG1vZHVsZSAnXCIraStcIidcIik7dGhyb3cgYS5jb2RlPVwiTU9EVUxFX05PVF9GT1VORFwiLGF9dmFyIHA9bltpXT17ZXhwb3J0czp7fX07ZVtpXVswXS5jYWxsKHAuZXhwb3J0cyxmdW5jdGlvbihyKXt2YXIgbj1lW2ldWzFdW3JdO3JldHVybiBvKG58fHIpfSxwLHAuZXhwb3J0cyxyLGUsbix0KX1yZXR1cm4gbltpXS5leHBvcnRzfWZvcih2YXIgdT1cImZ1bmN0aW9uXCI9PXR5cGVvZiByZXF1aXJlJiZyZXF1aXJlLGk9MDtpPHQubGVuZ3RoO2krKylvKHRbaV0pO3JldHVybiBvfXJldHVybiByfSkoKSIsImltcG9ydCB7IFZlaGljbGUgfSBmcm9tIFwiLi9WZWhpY2xlXCI7XHJcbmltcG9ydCB7IHRvZ2dsZUFsZXJ0IH0gZnJvbSBcIi4vYXBwXCI7XHJcblxyXG5leHBvcnQgY2xhc3MgRW1wbG95ZWUge1xyXG4gICAgcGFzc3dvcmQgPSAnbm9uZSc7XHJcbiAgICBzdHJvbmdSZWdleCA9IG5ldyBSZWdFeHAoXCJeKD89LipbYS16XSkoPz0uKltBLVpdKSg/PS4qWzAtOV0pKD89LipbIUAjXFwkJVxcXiZcXCpdKSg/PS57MTAsfSlcIik7XHJcbiAgICBtZWRpdW1SZWdleCA9IG5ldyBSZWdFeHAoXCJeKCgoPz0uKlthLXpdKSg/PS4qW0EtWl0pKXwoKD89LipbYS16XSkoPz0uKlswLTldKSl8KCg/PS4qW0EtWl0pKD89LipbMC05XSkpKSg/PS57Nyx9KVwiKTtcclxuICAgIGVub3VnaFJlZ2V4ID0gbmV3IFJlZ0V4cChcIig/PS57MCx9KS4qXCIpO1xyXG5cclxuICAgIG5hbWU6IHN0cmluZztcclxuICAgIGVtYWlsOiBzdHJpbmc7XHJcblxyXG4gICAgZm9ybUZpZWxkczogTm9kZUxpc3RPZjxIVE1MRWxlbWVudD47XHJcbiAgICBmb3JtQnRuOiBIVE1MRWxlbWVudDtcclxuXHJcbiAgICB0ZW1wbGF0ZTogc3RyaW5nO1xyXG4gICAgdmVoaWNsZTogVmVoaWNsZTtcclxuXHJcbiAgICBmbGFnRW1wbG95ZWVGb3JtID0gZmFsc2U7XHJcblxyXG4gICAgY29uc3RydWN0b3IoKSB7XHJcbiAgICAgICAgdGhpcy50ZW1wbGF0ZSA9IGBcclxuICAgICAgICAgICAgPGltZyBjbGFzcz1cImZvcm0taW1hZ2VcIiBzcmM9XCJpbWFnZXMvbWV0YS1lbXBsb3llZS5wbmdcIiBhbHQ9XCJFbXBsb3llZSBpbWFnZVwiPlxyXG4gICAgICAgICAgICA8Zm9ybSBpZD1cImVtcGxveWVlLWZvcm1cIj5cclxuICAgICAgICAgICAgICAgIDxzcGFuIGlkPVwiZW1wbG95ZWUtaW5wdXQtaGVhZGluZ1wiPldlbGNvbWUsIENhbiB3ZSBrbm93IHlvdXIgbmFtZT88L3NwYW4+XHJcbiAgICAgICAgICAgICAgICA8aW5wdXQgY2xhc3M9XCJjdXN0b20taW5wdXRcIiB0eXBlPVwidGV4dFwiIHBsYWNlaG9sZGVyPVwiRnVsbCBOYW1lXCIgLz5cclxuICAgICAgICAgICAgICAgIDxzZWxlY3QgaWQ9XCJzZWxlY3QtZ2VuZGVyXCIgbmFtZT1cImdlbmRlclwiIGNsYXNzPVwiY3VzdG9tLWlucHV0XCI+XHJcbiAgICAgICAgICAgICAgICAgICAgPG9wdGlvbiB2YWx1ZT1cIm5vbmVcIiBzZWxlY3RlZCBkaXNhYmxlZCBoaWRkZW4+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIFNlbGVjdCBHZW5kZXJcclxuICAgICAgICAgICAgICAgICAgICA8L29wdGlvbj5cclxuICAgICAgICAgICAgICAgICAgICA8b3B0aW9uIHZhbHVlPVwiTWFsZVwiPk1hbGU8L29wdGlvbj5cclxuICAgICAgICAgICAgICAgICAgICA8b3B0aW9uIHZhbHVlPVwiRmVtYWxlXCI+RmVtYWxlPC9vcHRpb24+XHJcbiAgICAgICAgICAgICAgICA8L3NlbGVjdD5cclxuICAgICAgICAgICAgICAgIDxpbnB1dCBuYW1lPVwiZW1haWxcIiBjbGFzcz1cImN1c3RvbS1pbnB1dFwiIHR5cGU9XCJlbWFpbFwiIHBsYWNlaG9sZGVyPVwiRW1haWxcIiAvPlxyXG4gICAgICAgICAgICAgICAgPGlucHV0IGlkPVwicGFzc3dvcmRcIiBuYW1lPVwicGFzc3dvcmRcIiBjbGFzcz1cImN1c3RvbS1pbnB1dFwiIHR5cGU9XCJwYXNzd29yZFwiIHBsYWNlaG9sZGVyPVwiUGFzc3dvcmRcIiAvPlxyXG4gICAgICAgICAgICAgICAgPGlucHV0IG5hbWU9XCJjb25maXJtLXBhc3N3b3JkXCIgY2xhc3M9XCJjdXN0b20taW5wdXRcIiB0eXBlPVwicGFzc3dvcmRcIiBwbGFjZWhvbGRlcj1cIkNvbmZpcm0gcGFzc3dvcmRcIiAvPlxyXG4gICAgICAgICAgICAgICAgPGlucHV0IGlkPVwiY29udGFjdFwiIG5hbWU9XCJjb250YWN0XCIgY2xhc3M9XCJjdXN0b20taW5wdXRcIiB0eXBlPVwibnVtYmVyXCIgcGxhY2Vob2xkZXI9XCJDb250YWN0XCJcclxuICAgICAgICAgICAgICAgICAgICBwYXR0ZXJuPVwiLns4LH1cIiB0aXRsZT1cIkVpZ2h0IG9yIG1vcmUgY2hhcmFjdGVyc1wiIHJlcXVpcmVkPlxyXG4gICAgICAgICAgICAgICAgPGJ1dHRvbiBjbGFzcz1cImZvcm0tYnV0dG9uXCIgdHlwZT1cImJ1dHRvblwiPlNVQk1JVDwvYnV0dG9uPlxyXG4gICAgICAgICAgICA8L2Zvcm0+XHJcbiAgICAgICAgYDtcclxuICAgICAgICB0aGlzLnZlaGljbGUgPSBuZXcgVmVoaWNsZSgpO1xyXG4gICAgfVxyXG5cclxuICAgIHZhbGlkYXRlKGlucHV0VHlwZTogc3RyaW5nLCB2YWx1ZTogc3RyaW5nKSB7XHJcbiAgICAgICAgc3dpdGNoIChpbnB1dFR5cGUpIHtcclxuICAgICAgICAgICAgY2FzZSBcInRleHRcIjpcclxuICAgICAgICAgICAgICAgIGlmICh2YWx1ZS5sZW5ndGggPCAyIHx8IHRoaXMuaXNOdW1lcmljKHZhbHVlKSkge1xyXG4gICAgICAgICAgICAgICAgICAgIGFsZXJ0KGBFbnRlciB2YWxpZCBOYW1lYCk7XHJcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIGZhbHNlO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgYnJlYWs7XHJcblxyXG4gICAgICAgICAgICBjYXNlIFwicGFzc3dvcmRcIjpcclxuICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKFwicGFzOiBcIiArIHZhbHVlKVxyXG4gICAgICAgICAgICAgICAgaWYgKHRoaXMucGFzc3dvcmQgIT0gJ25vbmUnKSB7XHJcblxyXG4gICAgICAgICAgICAgICAgICAgIGlmICh0aGlzLnBhc3N3b3JkID09IHZhbHVlKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKFwiQyBwYXM6IFwiICsgdmFsdWUpXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiB0cnVlO1xyXG4gICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiBmYWxzZTtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAgICAgaWYgKHZhbHVlLmxlbmd0aCA8IDggfHwgdmFsdWUuc2VhcmNoKC9bMC05XS8pIDwgMCB8fCB2YWx1ZS5zZWFyY2goL1thLXpdL2kpIDwgMCB8fCB2YWx1ZS5zZWFyY2goL1tBLVpdL2kpIDwgMCkge1xyXG4gICAgICAgICAgICAgICAgICAgIGFsZXJ0KGBzaG91bGQgY29udGFpbnMgVXBwZXJjYXNlLCBMb3dlcmNhc2UsIE51bWVyaWMsIEFscGhhbnVtZXJpYywgYW5kIGxlbmd0aCBtaW5pbXVtIDhgKTtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLnBhc3N3b3JkID0gJ25vbmUnO1xyXG4gICAgICAgICAgICAgICAgICAgIHJldHVybiBmYWxzZTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIGJyZWFrO1xyXG5cclxuICAgICAgICAgICAgY2FzZSBcImVtYWlsXCI6XHJcbiAgICAgICAgICAgICAgICB2YXIgYXRwb3NpdGlvbiA9IHZhbHVlLmluZGV4T2YoXCJAXCIpO1xyXG4gICAgICAgICAgICAgICAgdmFyIGRvdHBvc2l0aW9uID0gdmFsdWUubGFzdEluZGV4T2YoXCIuXCIpO1xyXG4gICAgICAgICAgICAgICAgaWYgKGF0cG9zaXRpb24gPCAxIHx8IGRvdHBvc2l0aW9uIDwgYXRwb3NpdGlvbiArIDIgfHwgZG90cG9zaXRpb24gKyAyID49IHZhbHVlLmxlbmd0aCkge1xyXG4gICAgICAgICAgICAgICAgICAgIHJldHVybiBmYWxzZTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIGJyZWFrO1xyXG5cclxuICAgICAgICAgICAgY2FzZSBcIm51bWJlclwiOlxyXG4gICAgICAgICAgICAgICAgY29uc29sZS5sb2codmFsdWUgKyBcIlwiICsgIXRoaXMuaXNOdW1lcmljKHZhbHVlKSB8fCB2YWx1ZS5sZW5ndGggPCA4KTtcclxuICAgICAgICAgICAgICAgIGlmICghdGhpcy5pc051bWVyaWModmFsdWUpIHx8IHZhbHVlLmxlbmd0aCA8IDgpIHtcclxuICAgICAgICAgICAgICAgICAgICBhbGVydChcIlBsZWFzZSBlbnRlciBhIHZhbGlkIGNvbnRhY3QgbnVtYmVyXCIpO1xyXG4gICAgICAgICAgICAgICAgICAgIHJldHVybiBmYWxzZTtcclxuICAgICAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIGNhc2UgXCJnZW5kZXJcIjpcclxuICAgICAgICAgICAgICAgIGlmICh2YWx1ZSA9PSAnbm9uZScpIHtcclxuICAgICAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyh2YWx1ZSArIFwiZ2VuZGVyXCIpO1xyXG4gICAgICAgICAgICAgICAgICAgIHJldHVybiBmYWxzZTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICAgICAgcmV0dXJuIHRydWU7XHJcbiAgICB9XHJcblxyXG4gICAgaXNOdW1lcmljKHZhbHVlOiBzdHJpbmcpIHtcclxuICAgICAgICByZXR1cm4gL14tP1xcZCskLy50ZXN0KHZhbHVlKTtcclxuICAgIH1cclxuXHJcbiAgICBwYXNzd29yZENoYW5nZWQoKSB7XHJcbiAgICAgICAgdmFyIHB3ZCA9IGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKFwicGFzc3dvcmRcIikgYXMgSFRNTElucHV0RWxlbWVudDtcclxuXHJcbiAgICAgICAgaWYgKHRoaXMuc3Ryb25nUmVnZXgudGVzdChwd2QudmFsdWUpKSB7XHJcbiAgICAgICAgICAgIHB3ZC5zdHlsZS5iYWNrZ3JvdW5kQ29sb3IgPSAnZ3JlZW4nO1xyXG4gICAgICAgIH0gZWxzZSBpZiAodGhpcy5tZWRpdW1SZWdleC50ZXN0KHB3ZC52YWx1ZSkpIHtcclxuICAgICAgICAgICAgcHdkLnN0eWxlLmJhY2tncm91bmRDb2xvciA9ICdvcmFuZ2UnO1xyXG4gICAgICAgIH0gZWxzZSBpZiAodGhpcy5lbm91Z2hSZWdleC50ZXN0KHB3ZC52YWx1ZSkpIHtcclxuICAgICAgICAgICAgcHdkLnN0eWxlLmJhY2tncm91bmRDb2xvciA9ICdyZWQnO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBpbml0aWFsaXplRW1wbG95ZWUoKSB7XHJcbiAgICAgICAgbGV0IGVtcGxveWVlRGl2ID0gZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoXCJlbXBsb3llZS1jb2xsYXBzaWJsZVwiKVxyXG4gICAgICAgIGVtcGxveWVlRGl2LmlubmVySFRNTCA9IHRoaXMudGVtcGxhdGU7XHJcbiAgICAgICAgdGhpcy5lbXBsb3llZUZ1bmN0aW9uKCk7XHJcbiAgICB9XHJcblxyXG4gICAgaW5pdGlhbGl6ZUZvcm0oKSB7XHJcbiAgICAgICAgdGhpcy5mb3JtRmllbGRzID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvckFsbChgI2VtcGxveWVlLWZvcm0gPiAuY3VzdG9tLWlucHV0YCk7IC8vIEdldCBhbGwgaW5wdXQgZmllbGRzXHJcbiAgICAgICAgZm9yIChsZXQgaSA9IDE7IGkgPCB0aGlzLmZvcm1GaWVsZHMubGVuZ3RoOyBpKyspIHtcclxuICAgICAgICAgICAgdGhpcy5mb3JtRmllbGRzW2ldLnN0eWxlLmRpc3BsYXkgPSBcIm5vbmVcIjsgLy8gU2V0IGRpc3BsYXkgbm9uZSB0byBoaWRlIGFsbCBpbnB1dHNcclxuICAgICAgICB9XHJcbiAgICAgICAgdGhpcy5mb3JtQnRuID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvcihgI2VtcGxveWVlLWZvcm0gYnV0dG9uYCk7XHJcbiAgICAgICAgdGhpcy5mb3JtQnRuLnN0eWxlLmRpc3BsYXkgPSBcIm5vbmVcIjtcclxuICAgIH1cclxuXHJcbiAgICBlbXBsb3llZUZ1bmN0aW9uKCkge1xyXG4gICAgICAgIGNvbnN0IGhlYWRlcl9zcGFuID0gZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoXCJlbXBsb3llZS1pbnB1dC1oZWFkaW5nXCIpO1xyXG4gICAgICAgIHRoaXMuaW5pdGlhbGl6ZUZvcm0oKTtcclxuXHJcbiAgICAgICAgbGV0IG5hbWU6IHN0cmluZztcclxuXHJcbiAgICAgICAgZm9yIChsZXQgaSA9IDA7IGkgPCB0aGlzLmZvcm1GaWVsZHMubGVuZ3RoIC0gMTsgaSsrKSB7XHJcbiAgICAgICAgICAgIGxldCBlbGVtZW50ID0gdGhpcy5mb3JtRmllbGRzW2ldIGFzIEhUTUxJbnB1dEVsZW1lbnQ7XHJcblxyXG4gICAgICAgICAgICBsZXQgcGFyZW50ID0gdGhpcztcclxuXHJcbiAgICAgICAgICAgIGVsZW1lbnQuYWRkRXZlbnRMaXN0ZW5lcihcImtleXByZXNzXCIsIGZ1bmN0aW9uIChldmVudCkge1xyXG4gICAgICAgICAgICAgICAgaWYgKGVsZW1lbnQubmFtZSA9PSBcInBhc3N3b3JkXCIpIHtcclxuICAgICAgICAgICAgICAgICAgICBwYXJlbnQucGFzc3dvcmRDaGFuZ2VkKCk7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICBpZiAoZWxlbWVudC52YWx1ZS5sZW5ndGggIT0gMCAmJlxyXG4gICAgICAgICAgICAgICAgICAgIGV2ZW50LmtleUNvZGUgPT0gMTMgJiYgcGFyZW50LnZhbGlkYXRlKGVsZW1lbnQudHlwZSwgZWxlbWVudC52YWx1ZSkgJiYgZWxlbWVudC52YWx1ZSAhPSAnbm9uZScpIHtcclxuICAgICAgICAgICAgICAgICAgICAvLyBJZiBlbnRlciBpcyBwcmVzc2VkIGFuZCBkYXRhIGlzIGVudGVyZWRcclxuICAgICAgICAgICAgICAgICAgICBsZXQgbmV4dEVsZW1lbnQgPSBlbGVtZW50Lm5leHRFbGVtZW50U2libGluZyBhcyBIVE1MSW5wdXRFbGVtZW50O1xyXG4gICAgICAgICAgICAgICAgICAgIGxldCBtc2cgPSBgQ2FuIHdlIGtub3cgeW91ciAke25leHRFbGVtZW50Lm5hbWV9P2A7XHJcbiAgICAgICAgICAgICAgICAgICAgY29uc29sZS5sb2cobXNnKTtcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKGVsZW1lbnQubmFtZSA9PSBcInBhc3N3b3JkXCIpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgcGFyZW50LnBhc3N3b3JkID0gZWxlbWVudC52YWx1ZTtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAgICAgICAgIGlmICghbmFtZSkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBuYW1lID0gZWxlbWVudC52YWx1ZTsgLy8gU2hvdyBIZWxsbyArIDxuYW1lPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICBtc2cgPSBgSGkgJHtuYW1lfSwgJHttc2d9YDtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAgICAgICAgIGhlYWRlcl9zcGFuLmlubmVySFRNTCA9IGAke21zZ31gO1xyXG4gICAgICAgICAgICAgICAgICAgIG5leHRFbGVtZW50LnN0eWxlLmRpc3BsYXkgPSBcImJsb2NrXCI7IC8vIFNob3cgbmV4dCBlbGVtZW50IGFuZCBmb2N1c1xyXG4gICAgICAgICAgICAgICAgICAgIGVsZW1lbnQuc3R5bGUuZGlzcGxheSA9IFwibm9uZVwiO1xyXG4gICAgICAgICAgICAgICAgICAgIG5leHRFbGVtZW50LmZvY3VzKCk7XHJcblxyXG4gICAgICAgICAgICAgICAgICAgIGlmIChpID09IHBhcmVudC5mb3JtRmllbGRzLmxlbmd0aCAtIDIpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgcGFyZW50LmZvcm1CdG4uc3R5bGUuZGlzcGxheSA9IFwiYmxvY2tcIjtcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHBhcmVudC5mb3JtQnRuLmFkZEV2ZW50TGlzdGVuZXIoXCJjbGlja1wiLCBmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZiAoIXBhcmVudC52YWxpZGF0ZShuZXh0RWxlbWVudC50eXBlLCBuZXh0RWxlbWVudC52YWx1ZSkpXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIGZhbHNlO1xyXG5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRvZ2dsZUFsZXJ0KFwiYmxvY2tcIiwgXCJUaGFua3MgZm9yIHRlbGxpbmcgdXMgYWJvdXQgeW91cnNlbGYsIFlvdXIgSUQgaXM6IDFcIiwgXCJhbGVydC1zdWNjZXNzXCIpO1xyXG5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKCdtZW51LWNoZWNrYm94LWVtcGxveWVlJykuY2xpY2soKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGhlYWRlcl9zcGFuLmlubmVySFRNTCA9IFwiVGhhbmtzIGZvciB0ZWxsaW5nIHVzIGFib3V0IHlvdXJzZWxmLCBZb3VyIElEIGlzOiAxXCI7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBwYXJlbnQuZmxhZ0VtcGxveWVlRm9ybSA9IHRydWU7XHJcblxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgcGFyZW50LmZvcm1CdG4uc3R5bGUuZGlzcGxheSA9IFwibm9uZVwiO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgbmV4dEVsZW1lbnQuc3R5bGUuZGlzcGxheSA9IFwibm9uZVwiO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgc2V0VGltZW91dChmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdG9nZ2xlQWxlcnQoXCJub25lXCIsIG51bGwsIG51bGwpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHBhcmVudC52ZWhpY2xlLmluaXRpYWxpemVWZWhpY2xlKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9LCAzMDAwKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIGlzRW1wbG95ZWVGb3JtRmlsbGVkKCkge1xyXG4gICAgICAgIHJldHVybiB0aGlzLmZsYWdFbXBsb3llZUZvcm07XHJcbiAgICB9XHJcbn0iLCJleHBvcnQgY2xhc3MgUGFzcyB7XHJcbiAgICBmbGFnUHJpY2luZzogYm9vbGVhbjtcclxuICAgIHZlaGljbGVUeXBlOiBzdHJpbmc7XHJcbiAgICBzZWxlY3RUeXBlOiBzdHJpbmc7XHJcbiAgICBjdXJyZW5jeTogc3RyaW5nO1xyXG4gICAgdGVtcGxhdGU6c3RyaW5nO1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKCkge1xyXG4gICAgICAgIHRoaXMuZmxhZ1ByaWNpbmcgPSBmYWxzZTtcclxuICAgICAgICB0aGlzLmN1cnJlbmN5ID0gXCJkb2xsYXJcIjtcclxuICAgICAgICB0aGlzLnRlbXBsYXRlID0gYDwhLS1DeWNsZS0tPlxyXG4gICAgICAgIDwhLS0gPHNwYW4gaWQ9XCJjeWNsZS1zcGFuLWZpbmFsLXByaWNlXCIgY2xhc3M9XCJmaW5hbC1wcmljZVwiPjwvc3Bhbj4gLS0+XHJcbiAgICAgICAgPGRpdiBjbGFzcz1cInZlaGljbGUtdHlwZVwiIGlkPVwiY3ljbGUtZGl2XCI+XHJcbiAgICAgICAgICAgIDxoMz5DWUNMRTwvaDM+XHJcbiAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJjaXJjbGUgY2VudGVyLWZsZXhcIj5cclxuICAgICAgICAgICAgICAgIDxsYWJlbCBpZD1cImN5Y2xlLXByaWNlXCIgY2xhc3M9XCJsYWJlbC1tb250aC1wcmljZVwiPiQxMDA8L2xhYmVsPlxyXG4gICAgICAgICAgICAgICAgPGxhYmVsIGlkPVwiY3ljbGUtZHVyYXRpb25cIj4vbW9udGg8L2xhYmVsPlxyXG4gICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgPHNlbGVjdCBpZD1cImN5Y2xlLWN1cnJlbmN5XCIgY2xhc3M9XCJjdXN0b20taW5wdXQgY2hhbmdlLWN1cnJcIj5cclxuICAgICAgICAgICAgICAgIDxvcHRpb24gdmFsdWU9XCJkb2xsYXJcIiBzZWxlY3RlZD4kIERvbGxhcjwvb3B0aW9uPlxyXG4gICAgICAgICAgICAgICAgPG9wdGlvbiB2YWx1ZT1cInllblwiPiYjMTY1OyBZRU48L29wdGlvbj5cclxuICAgICAgICAgICAgPC9zZWxlY3Q+XHJcbiAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJsYWJlbHMtcHJpY2UgY2VudGVyLWZsZXhcIj5cclxuICAgICAgICAgICAgICAgIDxzZWxlY3QgaWQ9XCJjeWNsZVwiIGNsYXNzPVwiY3VzdG9tLWlucHV0IHB1cmNoYXNlLWN1cnJcIj5cclxuICAgICAgICAgICAgICAgICAgICA8b3B0aW9uIHZhbHVlPVwibm9uZVwiIHNlbGVjdGVkIGRpc2FibGVkIGhpZGRlbj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgU2VsZWN0IFBsYW5cclxuICAgICAgICAgICAgICAgICAgICA8L29wdGlvbj5cclxuICAgICAgICAgICAgICAgICAgICA8b3B0aW9uIGRhdGEtcHJpY2U9XCI1XCIgLCBkYXRhLWR1cmF0aW9uPVwiZGFpbHlcIj5EYWlseTwvb3B0aW9uPlxyXG4gICAgICAgICAgICAgICAgICAgIDxvcHRpb24gZGF0YS1wcmljZT1cIjEwMFwiICwgZGF0YS1kdXJhdGlvbj1cIm1vbnRobHlcIj5Nb250aGx5PC9vcHRpb24+XHJcbiAgICAgICAgICAgICAgICAgICAgPG9wdGlvbiBkYXRhLXByaWNlPVwiNTAwXCIgLCBkYXRhLWR1cmF0aW9uPVwieWVhcmx5XCI+WWVhcmx5PC9vcHRpb24+XHJcbiAgICAgICAgICAgICAgICA8L3NlbGVjdD5cclxuICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgIDxidXR0b24gaWQ9XCJjeWNsZS1idXR0b25cIiB0eXBlPVwiYnV0dG9uXCI+R0VUIFBBU1M8L2J1dHRvbj5cclxuICAgICAgICA8L2Rpdj5cclxuXHJcbiAgICAgICAgPCEtLU1vdG9yIEN5Y2xlLS0+XHJcbiAgICAgICAgPCEtLSA8c3BhbiBpZD1cIm1vdG9yLWN5Y2xlLXNwYW4tZmluYWwtcHJpY2VcIiBjbGFzcz1cImZpbmFsLXByaWNlXCI+PC9zcGFuPiAtLT5cclxuICAgICAgICA8ZGl2IGNsYXNzPVwidmVoaWNsZS10eXBlXCIgaWQ9XCJtb3Rvci1jeWNsZS1kaXZcIj5cclxuICAgICAgICAgICAgPGgzPk1PVE9SIENZQ0xFPC9oMz5cclxuICAgICAgICAgICAgPGRpdiBjbGFzcz1cImNpcmNsZSBjZW50ZXItZmxleFwiPlxyXG4gICAgICAgICAgICAgICAgPGxhYmVsIGlkPVwibW90b3ItY3ljbGUtcHJpY2VcIiBjbGFzcz1cImxhYmVsLW1vbnRoLXByaWNlXCI+JDIwMDwvbGFiZWw+XHJcbiAgICAgICAgICAgICAgICA8bGFiZWwgaWQ9XCJtb3Rvci1jeWNsZS1kdXJhdGlvblwiPi9tb250aDwvbGFiZWw+XHJcbiAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICA8ZGl2IGNsYXNzPVwibGFiZWxzLXByaWNlIGNlbnRlci1mbGV4XCI+XHJcbiAgICAgICAgICAgICAgICA8c2VsZWN0IGlkPVwibW90b3ItY3ljbGUtY3VycmVuY3lcIiBjbGFzcz1cImN1c3RvbS1pbnB1dCBjaGFuZ2UtY3VyclwiPlxyXG4gICAgICAgICAgICAgICAgICAgIDxvcHRpb24gdmFsdWU9XCJkb2xsYXJcIiBzZWxlY3RlZD4kIERvbGxhcjwvb3B0aW9uPlxyXG4gICAgICAgICAgICAgICAgICAgIDxvcHRpb24gdmFsdWU9XCJ5ZW5cIj7CpSBZRU48L29wdGlvbj5cclxuICAgICAgICAgICAgICAgIDwvc2VsZWN0PlxyXG5cclxuICAgICAgICAgICAgICAgIDxzZWxlY3QgaWQ9XCJtb3Rvci1jeWNsZVwiIGNsYXNzPVwiY3VzdG9tLWlucHV0IHB1cmNoYXNlLWN1cnJcIj5cclxuICAgICAgICAgICAgICAgICAgICA8b3B0aW9uIHZhbHVlPVwibm9uZVwiIHNlbGVjdGVkIGRpc2FibGVkIGhpZGRlbj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgU2VsZWN0IFBsYW5cclxuICAgICAgICAgICAgICAgICAgICA8L29wdGlvbj5cclxuICAgICAgICAgICAgICAgICAgICA8b3B0aW9uIGRhdGEtcHJpY2U9XCIxMFwiICwgZGF0YS1kdXJhdGlvbj1cImRhaWx5XCI+RGFpbHk8L29wdGlvbj5cclxuICAgICAgICAgICAgICAgICAgICA8b3B0aW9uIGRhdGEtcHJpY2U9XCIyMDBcIiAsIGRhdGEtZHVyYXRpb249XCJtb250aGx5XCI+TW9udGhseTwvb3B0aW9uPlxyXG4gICAgICAgICAgICAgICAgICAgIDxvcHRpb24gZGF0YS1wcmljZT1cIjEwMDBcIiAsIGRhdGEtZHVyYXRpb249XCJ5ZWFybHlcIj5ZZWFybHk8L29wdGlvbj5cclxuICAgICAgICAgICAgICAgIDwvc2VsZWN0PlxyXG4gICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgPGJ1dHRvbiBpZD1cIm1vdG9yLWN5Y2xlLWJ1dHRvblwiIHR5cGU9XCJidXR0b25cIj5HRVQgUEFTUzwvYnV0dG9uPlxyXG4gICAgICAgIDwvZGl2PlxyXG5cclxuICAgICAgICA8IS0tRm91ciBXaGVlbGVyLS0+XHJcbiAgICAgICAgPCEtLSA8c3BhbiBpZD1cImZvdXItd2hlZWxlci1maW5hbC1wcmljZVwiIGNsYXNzPVwiZmluYWwtcHJpY2VcIj48L3NwYW4+IC0tPlxyXG4gICAgICAgIDxkaXYgY2xhc3M9XCJ2ZWhpY2xlLXR5cGVcIiBpZD1cImZvdXItd2hlZWxlci1kaXZcIj5cclxuICAgICAgICAgICAgPGgzPkZPVVIgV0hFRUxFUjwvaDM+XHJcbiAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJjaXJjbGUgY2VudGVyLWZsZXhcIj5cclxuICAgICAgICAgICAgICAgIDxsYWJlbCBpZD1cImZvdXItd2hlZWxlci1wcmljZVwiIGNsYXNzPVwibGFiZWwtbW9udGgtcHJpY2VcIj4kNTAwPC9sYWJlbD5cclxuICAgICAgICAgICAgICAgIDxsYWJlbCBpZD1cImZvdXItd2hlZWxlci1kdXJhdGlvblwiPi9tb250aDwvbGFiZWw+XHJcbiAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICA8c2VsZWN0IGlkPVwiZm91ci13aGVlbGVyLWN1cnJlbmN5XCIgY2xhc3M9XCJjdXN0b20taW5wdXQgY2hhbmdlLWN1cnJcIj5cclxuICAgICAgICAgICAgICAgIDxvcHRpb24gdmFsdWU9XCJkb2xsYXJcIiBzZWxlY3RlZD4kIERvbGxhcjwvb3B0aW9uPlxyXG4gICAgICAgICAgICAgICAgPG9wdGlvbiB2YWx1ZT1cInllblwiPiYjMTY1OyBZRU48L29wdGlvbj5cclxuICAgICAgICAgICAgPC9zZWxlY3Q+XHJcbiAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJsYWJlbHMtcHJpY2UgY2VudGVyLWZsZXhcIj5cclxuICAgICAgICAgICAgICAgIDxzZWxlY3QgaWQ9XCJmb3VyLXdoZWVsZXJcIiBjbGFzcz1cImN1c3RvbS1pbnB1dCBwdXJjaGFzZS1jdXJyXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgPG9wdGlvbiB2YWx1ZT1cIm5vbmVcIiBzZWxlY3RlZCBkaXNhYmxlZCBoaWRkZW4+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIFNlbGVjdCBQbGFuXHJcbiAgICAgICAgICAgICAgICAgICAgPC9vcHRpb24+XHJcbiAgICAgICAgICAgICAgICAgICAgPG9wdGlvbiBkYXRhLXByaWNlPVwiMjBcIiAsIGRhdGEtZHVyYXRpb249XCJkYWlseVwiPkRhaWx5PC9vcHRpb24+XHJcbiAgICAgICAgICAgICAgICAgICAgPG9wdGlvbiBkYXRhLXByaWNlPVwiNDAwXCIgLCBkYXRhLWR1cmF0aW9uPVwibW9udGhseVwiPk1vbnRobHk8L29wdGlvbj5cclxuICAgICAgICAgICAgICAgICAgICA8b3B0aW9uIGRhdGEtcHJpY2U9XCIyMDAwXCIgLCBkYXRhLWR1cmF0aW9uPVwieWVhcmx5XCI+WWVhcmx5PC9vcHRpb24+XHJcbiAgICAgICAgICAgICAgICA8L3NlbGVjdD5cclxuICAgICAgICAgICAgPC9kaXY+XHJcblxyXG4gICAgICAgICAgICA8YnV0dG9uIGlkPVwiZm91ci13aGVlbGVyLWJ1dHRvblwiIHR5cGU9XCJidXR0b25cIj5HRVQgUEFTUzwvYnV0dG9uPlxyXG4gICAgICAgIDwvZGl2PmA7XHJcbiAgICB9XHJcblxyXG4gICAgaW5pdGlhbGl6ZVBhc3ModlR5cGU6IHN0cmluZykge1xyXG4gICAgICAgIHRoaXMudmVoaWNsZVR5cGUgPSB2VHlwZTtcclxuICAgICAgICBsZXQgdmVoaWNsZURpdiA9IGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKFwicHJpY2luZy1saXN0XCIpO1xyXG4gICAgICAgIHZlaGljbGVEaXYuaW5uZXJIVE1MID0gdGhpcy50ZW1wbGF0ZTtcclxuICAgICAgICBsZXQgcGFyZW50ID0gdGhpcztcclxuXHJcbiAgICAgICAgbGV0IGNoYW5nZUN1cnIgPSBmdW5jdGlvbigpOiB2b2lkIHtcclxuICAgICAgICAgICAgaWYgKHRoaXMuY3VycmVuY3kgPT0gKFwiZG9sbGFyXCIpKVxyXG4gICAgICAgICAgICAgICAgdGhpcy5jdXJyZW5jeSA9IFwieWVuXCI7XHJcbiAgICAgICAgICAgIGVsc2VcclxuICAgICAgICAgICAgICAgIHRoaXMuY3VycmVuY3kgPSBcImRvbGxhclwiO1xyXG4gICAgICAgIH07XHJcblxyXG4gICAgICAgIGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3JBbGwoJy5jaGFuZ2UtY3VycicpLmZvckVhY2goaXRlbSA9PiB7XHJcbiAgICAgICAgICAgIGl0ZW0uYWRkRXZlbnRMaXN0ZW5lcignY2hhbmdlJywgY2hhbmdlQ3VycilcclxuICAgICAgICB9KVxyXG5cclxuICAgICAgICBkb2N1bWVudC5xdWVyeVNlbGVjdG9yQWxsKCcucHVyY2hhc2UtY3VycicpLmZvckVhY2goaXRlbSA9PiB7XHJcbiAgICAgICAgICAgIGl0ZW0uYWRkRXZlbnRMaXN0ZW5lcihcImNoYW5nZVwiLCBfID0+IHtcclxuICAgICAgICAgICAgICAgIHZhciBpZCA9IGl0ZW0uaWQ7XHJcbiAgICAgICAgICAgICAgICB2YXIgY3VycmVuY3lTeW1ib2wgPSBwYXJlbnQuY3VycmVuY3kgPT0gXCJkb2xsYXJcIiA/IFwiJFwiIDogJ8KlJztcclxuXHJcbiAgICAgICAgICAgICAgICB2YXIgc2VsZWN0ZWRJbmRleCA9IChkb2N1bWVudC5nZXRFbGVtZW50QnlJZChpZCkgYXMgSFRNTFNlbGVjdEVsZW1lbnQpLnNlbGVjdGVkSW5kZXg7XHJcbiAgICAgICAgICAgICAgICB2YXIgc2VsZWN0ZWRPcHRpb24gPSAoZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoaWQpIGFzIEhUTUxTZWxlY3RFbGVtZW50KS5vcHRpb25zW3NlbGVjdGVkSW5kZXhdO1xyXG5cclxuICAgICAgICAgICAgICAgIHZhciBwcmljZSA9IHBhcnNlSW50KHNlbGVjdGVkT3B0aW9uLmRhdGFzZXQucHJpY2UpO1xyXG4gICAgICAgICAgICAgICAgaWYgKGN1cnJlbmN5U3ltYm9sICE9IFwiJFwiKVxyXG4gICAgICAgICAgICAgICAgICAgIHByaWNlID0gcHJpY2UgLyAyO1xyXG5cclxuICAgICAgICAgICAgICAgIGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKGAke2lkfS1wcmljZWApLnRleHRDb250ZW50ID0gYCR7Y3VycmVuY3lTeW1ib2x9JHtwcmljZX1gO1xyXG4gICAgICAgICAgICAgICAgZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoYCR7aWR9LWR1cmF0aW9uYCkudGV4dENvbnRlbnQgPSBgLyR7c2VsZWN0ZWRPcHRpb24uZGF0YXNldC5kdXJhdGlvbn1gO1xyXG5cclxuICAgICAgICAgICAgICAgIHZhciBidG4gPSBkb2N1bWVudC5nZXRFbGVtZW50QnlJZChgJHtpZH0tYnV0dG9uYCk7XHJcbiAgICAgICAgICAgICAgICBidG4uc3R5bGUuZGlzcGxheSA9IFwiYmxvY2tcIjtcclxuXHJcbiAgICAgICAgICAgICAgICBidG4ub25jbGljayA9IGZ1bmN0aW9uICgpIHtcclxuICAgICAgICAgICAgICAgICAgICBkb2N1bWVudC5nZXRFbGVtZW50QnlJZChgJHtpZH0tZGl2YCkuc3R5bGUuZGlzcGxheSA9ICdub25lJztcclxuICAgICAgICAgICAgICAgICAgICBkb2N1bWVudC5nZXRFbGVtZW50QnlJZChcInByaWNpbmctbGlzdFwiKS5pbm5lckhUTUwgPSBgPGgyPiBGaW5hbCBQcmljZTogICQgJHtzZWxlY3RlZE9wdGlvbi5kYXRhc2V0LnByaWNlfSA8L2gyPmA7XHJcbiAgICAgICAgICAgICAgICB9O1xyXG4gICAgICAgICAgICB9KVxyXG4gICAgICAgIH0pXHJcbiAgICAgICAgdGhpcy5wcmljaW5nRnVuY3Rpb24oKTtcclxuICAgIH1cclxuXHJcblxyXG4gICAgcHJpY2luZ0Z1bmN0aW9uKCkge1xyXG4gICAgICAgIGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKCdtZW51LWNoZWNrYm94LXByaWNpbmcnKS5jbGljaygpO1xyXG5cclxuICAgICAgICB2YXIgdmVoaWNsZXMgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yQWxsKGAudmVoaWNsZS10eXBlYCkgYXMgTm9kZUxpc3RPZjxIVE1MRWxlbWVudD47XHJcbiAgICAgICAgZm9yIChsZXQgaSA9IDA7IGkgPCB2ZWhpY2xlcy5sZW5ndGg7IGkrKykge1xyXG4gICAgICAgICAgICBpZiAodmVoaWNsZXNbaV0gIT0gZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoYCR7dGhpcy52ZWhpY2xlVHlwZX0tZGl2YCkpXHJcbiAgICAgICAgICAgICAgICB2ZWhpY2xlc1tpXS5zdHlsZS5kaXNwbGF5ID0gXCJub25lXCI7IC8vIFNldCBkaXNwbGF5IG5vbmUgdG8gaGlkZSBhbGwgaW5wdXRzXHJcbiAgICAgICAgfVxyXG4gICAgICAgIChkb2N1bWVudC5nZXRFbGVtZW50QnlJZChgJHt0aGlzLnZlaGljbGVUeXBlfS1idXR0b25gKWFzIEhUTUxCdXR0b25FbGVtZW50KS5zdHlsZS5kaXNwbGF5ID0gXCJub25lXCI7XHJcbiAgICB9XHJcbn0iLCJpbXBvcnQgeyBQYXNzIH0gZnJvbSBcIi4vUGFzc1wiO1xyXG5pbXBvcnQgeyB0b2dnbGVBbGVydCB9IGZyb20gXCIuL2FwcFwiO1xyXG5cclxuZXhwb3J0IGNsYXNzIFZlaGljbGUge1xyXG5cclxuICAgIGZvcm1GaWVsZHM6IE5vZGVMaXN0T2Y8SFRNTEVsZW1lbnQ+O1xyXG4gICAgZm9ybUJ0bjogSFRNTEVsZW1lbnQ7XHJcbiAgICB2ZWhpY2xlVHlwZTogc3RyaW5nO1xyXG4gICAgdGVtcGxhdGU6IHN0cmluZztcclxuICAgIHBhc3M6IFBhc3M7XHJcblxyXG4gICAgc3RhdGljIGZsYWdWZWhpY2xlRm9ybTogYm9vbGVhbiA9IGZhbHNlO1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKCkge1xyXG4gICAgICAgIHRoaXMudGVtcGxhdGUgPSBgXHJcbiAgICAgICAgICAgIDxmb3JtIGlkPVwidmVoaWNsZS1mb3JtXCI+XHJcbiAgICAgICAgICAgIDxzcGFuIGlkPVwidmVoaWNsZS1pbnB1dC1oZWFkaW5nXCI+Q2FuIHdlIGtub3cgYWJvdXQgeW91ciB2ZWhpY2xlPzwvc3Bhbj5cclxuICAgICAgICAgICAgPGlucHV0IG5hbWU9XCJ2ZWhpY2xlIG1ha2VcIiBjbGFzcz1cImN1c3RvbS1pbnB1dFwiIHR5cGU9XCJ0ZXh0XCIgcGxhY2Vob2xkZXI9XCJWZWhpY2xlIE1ha2UgKENvbXBhbnkpXCJcclxuICAgICAgICAgICAgICAgIHJlcXVpcmVkIC8+XHJcbiAgICAgICAgICAgIDxpbnB1dCBuYW1lPVwidmVoaWNsZSBtb2RlbFwiIGNsYXNzPVwiY3VzdG9tLWlucHV0XCIgdHlwZT1cInRleHRcIiBwbGFjZWhvbGRlcj1cIlZlaGljbGUgTW9kZWxcIiByZXF1aXJlZCAvPlxyXG5cclxuICAgICAgICAgICAgPHNlbGVjdCBpZD1cInZlaGljbGUtdHlwZVwiIG5hbWU9XCJ0eXBlXCIgY2xhc3M9XCJjdXN0b20taW5wdXRcIj5cclxuICAgICAgICAgICAgICAgIDxvcHRpb24gdmFsdWU9XCJub25lXCIgc2VsZWN0ZWQgZGlzYWJsZWQgaGlkZGVuPlxyXG4gICAgICAgICAgICAgICAgICAgIFNlbGVjdCBWZWhpY2xlIFR5cGVcclxuICAgICAgICAgICAgICAgIDwvb3B0aW9uPlxyXG4gICAgICAgICAgICAgICAgPG9wdGlvbiB2YWx1ZT1cImN5Y2xlXCI+Q3ljbGU8L29wdGlvbj5cclxuICAgICAgICAgICAgICAgIDxvcHRpb24gdmFsdWU9XCJtb3Rvci1jeWNsZVwiPlR3byBXaGVlbGVyPC9vcHRpb24+XHJcbiAgICAgICAgICAgICAgICA8b3B0aW9uIHZhbHVlPVwiZm91ci13aGVlbGVyXCI+Rm91ciBXaGVlbGVyPC9vcHRpb24+XHJcbiAgICAgICAgICAgIDwvc2VsZWN0PlxyXG5cclxuICAgICAgICAgICAgPGlucHV0IG5hbWU9XCJ2ZWhpY2xlIG51bWJlclwiIGNsYXNzPVwiY3VzdG9tLWlucHV0XCIgdHlwZT1cInRleHRcIiBwbGFjZWhvbGRlcj1cIlZlaGljbGUgTnVtYmVyXCIgcmVxdWlyZWQgLz5cclxuICAgICAgICAgICAgPGlucHV0IG5hbWU9XCJlbXBsb3llZSBpZFwiIGNsYXNzPVwiY3VzdG9tLWlucHV0XCIgdHlwZT1cInRleHRcIiBwbGFjZWhvbGRlcj1cIkVtcGxveWVlIElkXCIgaWQ9XCJlbXBsb3llZS1pZFwiXHJcbiAgICAgICAgICAgICAgICByZXF1aXJlZCAvPlxyXG4gICAgICAgICAgICA8dGV4dGFyZWEgbmFtZT1cInZlaGljbGUgaWRlbnRpZmljYXRpb25cIiBjbGFzcz1cImN1c3RvbS1pbnB1dFwiXHJcbiAgICAgICAgICAgICAgICBwbGFjZWhvbGRlcj1cIklkZW50aWZpY2F0aW9uIChDb2xvciwgTWFyaywgVmlzaWJsZSBBY2Nlc3NvcnkgZXRjLik6XCI+PC90ZXh0YXJlYT5cclxuICAgICAgICAgICAgPGJ1dHRvbiBjbGFzcz1cImZvcm0tYnV0dG9uXCIgdHlwZT1cImJ1dHRvblwiPlNVQk1JVDwvYnV0dG9uPlxyXG4gICAgICAgIDwvZm9ybT5cclxuICAgICAgICA8aW1nIGNsYXNzPVwiZm9ybS1pbWFnZVwiIHNyYz1cImltYWdlcy9tZXRhLXZlaGljbGUucG5nXCIgYWx0PVwiVmVoaWNsZSBpbWFnZVwiPlxyXG4gICAgICAgIGA7XHJcblxyXG4gICAgICAgIHRoaXMucGFzcyA9IG5ldyBQYXNzKCk7XHJcbiAgICB9XHJcblxyXG4gICAgaW5pdGlhbGl6ZVZlaGljbGUoKSB7XHJcbiAgICAgICAgbGV0IHZlaGljbGVEaXYgPSBkb2N1bWVudC5nZXRFbGVtZW50QnlJZChcInZlaGljbGUtY29sbGFwc2libGVcIik7XHJcbiAgICAgICAgdmVoaWNsZURpdi5pbm5lckhUTUwgPSB0aGlzLnRlbXBsYXRlO1xyXG4gICAgICAgIHRoaXMudmVoaWNsZUZ1bmN0aW9uKCk7XHJcbiAgICB9XHJcblxyXG4gICAgaW5pdGlhbGl6ZUZvcm0oKSB7XHJcbiAgICAgICAgdGhpcy5mb3JtRmllbGRzID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvckFsbChgI3ZlaGljbGUtZm9ybSA+IC5jdXN0b20taW5wdXRgKTsgLy8gR2V0IGFsbCBpbnB1dCBmaWVsZHNcclxuICAgICAgICBmb3IgKGxldCBpID0gMTsgaSA8IHRoaXMuZm9ybUZpZWxkcy5sZW5ndGg7IGkrKykge1xyXG4gICAgICAgICAgICB0aGlzLmZvcm1GaWVsZHNbaV0uc3R5bGUuZGlzcGxheSA9IFwibm9uZVwiOyAvLyBTZXQgZGlzcGxheSBub25lIHRvIGhpZGUgYWxsIGlucHV0c1xyXG4gICAgICAgIH1cclxuICAgICAgICB0aGlzLmZvcm1CdG4gPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKGAjdmVoaWNsZS1mb3JtIGJ1dHRvbmApO1xyXG4gICAgICAgIHRoaXMuZm9ybUJ0bi5zdHlsZS5kaXNwbGF5ID0gXCJub25lXCI7XHJcbiAgICB9XHJcblxyXG4gICAgdmVoaWNsZUZ1bmN0aW9uKCkge1xyXG4gICAgICAgIGNvbnN0IGhlYWRlcl9zcGFuID0gZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoXCJ2ZWhpY2xlLWlucHV0LWhlYWRpbmdcIik7XHJcbiAgICAgICAgdGhpcy5pbml0aWFsaXplRm9ybSgpO1xyXG4gICAgICAgIGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKCdtZW51LWNoZWNrYm94LXZlaGljbGUnKS5jbGljaygpO1xyXG5cclxuICAgICAgICBmb3IgKGxldCBpID0gMDsgaSA8IHRoaXMuZm9ybUZpZWxkcy5sZW5ndGggLSAxOyBpKyspIHtcclxuICAgICAgICAgICAgbGV0IGVsZW1lbnQgPSB0aGlzLmZvcm1GaWVsZHNbaV0gYXMgSFRNTElucHV0RWxlbWVudDtcclxuXHJcbiAgICAgICAgICAgIGxldCBwYXJlbnQgPSB0aGlzO1xyXG5cclxuICAgICAgICAgICAgZWxlbWVudC5hZGRFdmVudExpc3RlbmVyKFwia2V5cHJlc3NcIiwgZnVuY3Rpb24gKGV2ZW50KSB7XHJcbiAgICAgICAgICAgICAgICBpZiAoZWxlbWVudC52YWx1ZS5sZW5ndGggIT0gMCAmJlxyXG4gICAgICAgICAgICAgICAgICAgIGV2ZW50LmtleUNvZGUgPT0gMTMgJiYgZWxlbWVudC52YWx1ZSAhPSAnbm9uZScpIHsgLy8gSWYgZW50ZXIgaXMgcHJlc3NlZCBhbmQgZGF0YSBpcyBlbnRlcmVkXHJcbiAgICAgICAgICAgICAgICAgICAgbGV0IG5leHRFbGVtZW50ID0gZWxlbWVudC5uZXh0RWxlbWVudFNpYmxpbmcgYXMgSFRNTElucHV0RWxlbWVudDtcclxuICAgICAgICAgICAgICAgICAgICBsZXQgbXNnID0gYENhbiB3ZSBrbm93IHlvdXIgJHtuZXh0RWxlbWVudC5uYW1lfT9gO1xyXG4gICAgICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKG1zZyk7XHJcblxyXG4gICAgICAgICAgICAgICAgICAgIGlmIChlbGVtZW50Lm5hbWUgPT0gXCJ0eXBlXCIpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgcGFyZW50LnZlaGljbGVUeXBlID0gZWxlbWVudC52YWx1ZTtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAgICAgICAgIGhlYWRlcl9zcGFuLmlubmVySFRNTCA9IGAke21zZ31gO1xyXG4gICAgICAgICAgICAgICAgICAgIG5leHRFbGVtZW50LnN0eWxlLmRpc3BsYXkgPSBcImJsb2NrXCI7IC8vIFNob3cgbmV4dCBlbGVtZW50IGFuZCBmb2N1c1xyXG4gICAgICAgICAgICAgICAgICAgIGVsZW1lbnQuc3R5bGUuZGlzcGxheSA9IFwibm9uZVwiO1xyXG4gICAgICAgICAgICAgICAgICAgIG5leHRFbGVtZW50LmZvY3VzKCk7XHJcblxyXG4gICAgICAgICAgICAgICAgICAgIGlmIChlbGVtZW50LmlkID09IFwidmVoaWNsZS10eXBlXCIpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgcGFyZW50LnZlaGljbGVUeXBlID0gZWxlbWVudC52YWx1ZTtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAgICAgICAgIGlmIChpID09IHBhcmVudC5mb3JtRmllbGRzLmxlbmd0aCAtIDIpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgcGFyZW50LmZvcm1CdG4uc3R5bGUuZGlzcGxheSA9IFwiYmxvY2tcIjtcclxuICAgICAgICAgICAgICAgICAgICAgICAgcGFyZW50LmZvcm1CdG4uYWRkRXZlbnRMaXN0ZW5lcihcImNsaWNrXCIsIGZ1bmN0aW9uICgpIHtcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0b2dnbGVBbGVydChcImJsb2NrXCIsIFwiVGhhbmtzIGZvciB0ZWxsaW5nIHVzIGFib3V0IHlvdXIgdmVoaWNsZVwiLCBcImFsZXJ0LXN1Y2Nlc3NcIik7XHJcblxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoJ21lbnUtY2hlY2tib3gtdmVoaWNsZScpLmNsaWNrKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBoZWFkZXJfc3Bhbi5pbm5lckhUTUwgPSBcIlRoYW5rcyBmb3IgdGVsbGluZyB1cyBhYm91dCB5b3VyIHZlaGljbGVcIjtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIFZlaGljbGUuZmxhZ1ZlaGljbGVGb3JtID0gdHJ1ZTtcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBwYXJlbnQuZm9ybUJ0bi5zdHlsZS5kaXNwbGF5ID0gXCJub25lXCI7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBuZXh0RWxlbWVudC5zdHlsZS5kaXNwbGF5ID0gXCJub25lXCI7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBzZXRUaW1lb3V0KGZ1bmN0aW9uICgpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0b2dnbGVBbGVydChcIm5vbmVcIiwgbnVsbCwgbnVsbCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgcGFyZW50LnBhc3MuaW5pdGlhbGl6ZVBhc3MocGFyZW50LnZlaGljbGVUeXBlKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0sIDMwMDApO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBzdGF0aWMgaXNWZWhpY2xlRm9ybUZpbGxlZCgpIHtcclxuICAgICAgICByZXR1cm4gVmVoaWNsZS5mbGFnVmVoaWNsZUZvcm07XHJcbiAgICB9XHJcbn0iLCJpbXBvcnQgeyBFbXBsb3llZSB9IGZyb20gXCIuL0VtcGxveWVlXCI7XHJcbmltcG9ydCB7IFZlaGljbGUgfSBmcm9tIFwiLi9WZWhpY2xlXCI7XHJcblxyXG5jb25zdCBhbGVydERpdiA9IGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKFwiYWxlcnQtZGl2XCIpO1xyXG5hbGVydERpdi5zdHlsZS5kaXNwbGF5ID0gXCJub25lXCI7XHJcblxyXG50b2dnbGVBbGVydChcIm5vbmVcIiwgbnVsbCwgXCJhbGVydC13YXJuaW5nXCIpO1xyXG5cclxubGV0IGVtcGxveWVlID0gbmV3IEVtcGxveWVlKCk7XHJcbmVtcGxveWVlLmluaXRpYWxpemVFbXBsb3llZSgpO1xyXG5cclxuZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoXCJ2ZWhpY2xlLWNvbGxhcHNlLXRvZ2dsZVwiKS5hZGRFdmVudExpc3RlbmVyKFwiY2xpY2tcIiwgZnVuY3Rpb24gKCkge1xyXG4gICAgaWYgKCFlbXBsb3llZS5pc0VtcGxveWVlRm9ybUZpbGxlZCgpKSB7XHJcbiAgICAgICAgZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoJ21lbnUtY2hlY2tib3gtdmVoaWNsZScpLmNsaWNrKCk7XHJcbiAgICAgICAgdG9nZ2xlQWxlcnQoXCJibG9ja1wiLCBcIlBsZWFzZSBmaWxsIHRoZSBFbXBsb3llZSBmb3JtIGZpcnN0IVwiLCBcImFsZXJ0LXdhcm5pbmdcIik7XHJcbiAgICAgICAgc2V0VGltZW91dChmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgICAgIHRvZ2dsZUFsZXJ0KFwibm9uZVwiLCBudWxsLCBudWxsKTtcclxuICAgICAgICB9LCAzMDAwKTtcclxuICAgIH1cclxufSk7XHJcblxyXG5kb2N1bWVudC5nZXRFbGVtZW50QnlJZChcInByaWNpbmctY29sbGFwc2UtdG9nZ2xlXCIpLmFkZEV2ZW50TGlzdGVuZXIoXCJjbGlja1wiLCBmdW5jdGlvbiAoKSB7XHJcbiAgICBpZiAoIVZlaGljbGUuaXNWZWhpY2xlRm9ybUZpbGxlZCgpKSB7XHJcbiAgICAgICAgZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoJ21lbnUtY2hlY2tib3gtcHJpY2luZycpLmNsaWNrKCk7XHJcbiAgICAgICAgdG9nZ2xlQWxlcnQoXCJibG9ja1wiLCBcIlBsZWFzZSBmaWxsIHRoZSBWZWhpY2xlIGZvcm0gZmlyc3QhXCIsIFwiYWxlcnQtd2FybmluZ1wiKTtcclxuICAgICAgICBzZXRUaW1lb3V0KGZ1bmN0aW9uICgpIHtcclxuICAgICAgICAgICAgdG9nZ2xlQWxlcnQoXCJub25lXCIsIG51bGwsIG51bGwpO1xyXG4gICAgICAgIH0sIDMwMDApO1xyXG4gICAgfVxyXG59KTtcclxuXHJcbmV4cG9ydCBmdW5jdGlvbiB0b2dnbGVBbGVydChkaXNwbGF5OiBzdHJpbmcsIG1zZzogc3RyaW5nLCBjbGFzc1R5cGU6IHN0cmluZykge1xyXG4gICAgYWxlcnREaXYucmVtb3ZlQXR0cmlidXRlKFwiY2xhc3NcIik7XHJcbiAgICBhbGVydERpdi5jbGFzc0xpc3QuYWRkKFwiYWxlcnRcIik7XHJcbiAgICBhbGVydERpdi5jbGFzc0xpc3QuYWRkKGNsYXNzVHlwZSk7XHJcblxyXG4gICAgYWxlcnREaXYuaW5uZXJIVE1MID0gbXNnO1xyXG4gICAgYWxlcnREaXYuc3R5bGUuZGlzcGxheSA9IGRpc3BsYXk7XHJcbn1cclxuIl19
