const express = require("express")
const http = require("http")
const app = express()
const server = http.Server(app)
const port = 3002;

app.use(express.static(__dirname + '/public/'));

app.listen(port, () => {
    console.log(`Example app listening on port ${port}!`)
});
