import { Vehicle } from "./Vehicle.js";
import { toggleAlert } from "./app.js";

export class Employee {
    password = 'none';
    strongRegex = new RegExp("^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{10,})");
    mediumRegex = new RegExp("^(((?=.*[a-z])(?=.*[A-Z]))|((?=.*[a-z])(?=.*[0-9]))|((?=.*[A-Z])(?=.*[0-9])))(?=.{7,})");
    enoughRegex = new RegExp("(?=.{0,}).*");

    name = "";
    email = "";

    formFields = null;
    formBtn = null;

    vehicle = null;

    flagEmployeeForm = false;

    constructor() {
        this.template = `
            <img class="form-image" src="images/meta-employee.png" alt="Employee image">
            <form id="employee-form">
                <span id="employee-input-heading">Welcome, Can we know your name?</span>
                <input class="custom-input" type="text" placeholder="Full Name" />
                <select id="select-gender" name="gender" class="custom-input">
                    <option value="none" selected disabled hidden>
                        Select Gender
                    </option>
                    <option value="Male">Male</option>
                    <option value="Female">Female</option>
                </select>
                <input name="email" class="custom-input" type="email" placeholder="Email" />
                <input id="password" name="password" class="custom-input" type="password" placeholder="Password" />
                <input name="confirm-password" class="custom-input" type="password" placeholder="Confirm password" />
                <input id="contact" name="contact" class="custom-input" type="number" placeholder="Contact"
                    pattern=".{8,}" title="Eight or more characters" required>
                <button class="form-button" type="button">SUBMIT</button>
            </form>
        `;
        this.vehicle = new Vehicle();
    }

    validate(inputType, value) {
        switch (inputType) {
            case "text":
                if (value.length < 2 || this.isNumeric(value)) {
                    alert(`Enter valid Name`);
                    return false;
                }
                break;

            case "password":
                console.log("pas: " + value)
                if (this.password != 'none') {

                    if (this.password == value) {
                        console.log("C pas: " + value)
                        return true;
                    } else {
                        return false;
                    }
                }

                if (value.length < 8 || value.search(/[0-9]/) < 0 || value.search(/[a-z]/i) < 0 || value.search(/[A-Z]/i) < 0) {
                    alert(`should contains Uppercase, Lowercase, Numeric, Alphanumeric, and length minimum 8`);
                    this.password = 'none';
                    return false;
                }
                break;

            case "email":
                var atposition = value.indexOf("@");
                var dotposition = value.lastIndexOf(".");
                if (atposition < 1 || dotposition < atposition + 2 || dotposition + 2 >= value.length) {
                    return false;
                }
                break;

            case "number":
                console.log(value + "" + !this.isNumeric(value) || value.length < 8);
                if (!this.isNumeric(value) || value.length < 8) {
                    alert("Please enter a valid contact number");
                    return false;
                }

            case "gender":
                if (value == 'none') {
                    console.log(value + "gender");
                    return false;
                }
        }
        return true;
    }

    isNumeric(value) {
        return /^-?\d+$/.test(value);
    }

    passwordChanged() {
        var pwd = document.getElementById("password");

        if (this.strongRegex.test(pwd.value)) {
            pwd.style.backgroundColor = 'green';
        } else if (this.mediumRegex.test(pwd.value)) {
            pwd.style.backgroundColor = 'orange';
        } else if (this.enoughRegex.test(pwd.value)) {
            pwd.style.backgroundColor = 'red';
        }
    }

    setEmail(email) {
        if (this.validate()) {
            this.email = email;
            return true;
        }
        return false;
    }

    initializeEmployee() {
        let employeeDiv = document.getElementById("employee-collapsible")
        employeeDiv.innerHTML = this.template;
        this.employeeFunction();
    }

    initializeForm() {
        this.formFields = document.querySelectorAll(`#employee-form > .custom-input`); // Get all input fields
        for (let i = 1; i < this.formFields.length; i++) {
            this.formFields[i].style.display = "none"; // Set display none to hide all inputs
        }
        this.formBtn = document.querySelector(`#employee-form button`);
        this.formBtn.style.display = "none";
    }

    employeeFunction() {
        const header_span = document.getElementById("employee-input-heading");
        this.initializeForm();

        let name = null;

        for (let i = 0; i < this.formFields.length - 1; i++) {
            let element = this.formFields[i];

            parent = this;

            element.addEventListener("keypress", function (event) {
                if (element.name == "password") {
                    parent.passwordChanged();
                }
                if (element.value.length != 0 &&
                    event.keyCode == 13 && parent.validate(element.type, element.value) && element.value != 'none') {
                    // If enter is pressed and data is entered
                    let nextElement = element.nextElementSibling;
                    let msg = `Can we know your ${nextElement.name}?`;
                    console.log(msg);

                    if (element.name == "password") {
                        this.password = element.value;
                    }

                    if (!name) {
                        name = element.value; // Show Hello + <name>
                        msg = `Hi ${name}, ${msg}`;
                    }

                    header_span.innerHTML = `${msg}`;
                    nextElement.style.display = "block"; // Show next element and focus
                    element.style.display = "none";
                    nextElement.focus();

                    if (i == parent.formFields.length - 2) {
                        parent.formBtn.style.display = "block";

                        parent.formBtn.addEventListener("click", function () {
                            if (!parent.validate(nextElement.type, nextElement.value))
                                return false;

                            toggleAlert("block", "Thanks for telling us about yourself, Your ID is: 1", "alert-success");

                            $('#menu-checkbox-employee').click();
                            header_span.innerHTML = "Thanks for telling us about yourself, Your ID is: 1";
                            parent.flagEmployeeForm = true;

                            parent.formBtn.style.display = "none";
                            nextElement.style.display = "none";
                            setTimeout(function () {
                                toggleAlert("none", null, null);
                                parent.vehicle.initializeVehicle();
                            }, 3000);
                        });
                    }

                }
            });
        }
    }

    isEmployeeFormFilled() {
        return this.flagEmployeeForm;
    }
}