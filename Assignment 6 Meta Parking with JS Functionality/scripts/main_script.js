var flagEmployeeForm = false;
var flagVehicleForm = false;
var flagPricing = false;
var vehicleType;
var currency = "dollar";

var formBtn, formFields;
var password;

function isNumeric(value) {
    return /^-?\d+$/.test(value);
}

function passwordChanged() {
    var strongRegex = new RegExp("^(?=.{14,})(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9])(?=.*\\W).*$", "g");
    var mediumRegex = new RegExp("^(?=.{10,})(((?=.*[A-Z])(?=.*[a-z]))|((?=.*[A-Z])(?=.*[0-9]))|((?=.*[a-z])(?=.*[0-9]))).*$", "g");
    var enoughRegex = new RegExp("(?=.{8,}).*", "g");
    var pwd = document.getElementById("password");
    if (strongRegex.test(pwd.value)) {
        pwd.style.backgroundColor = 'green';
    } else if (mediumRegex.test(pwd.value)) {
        pwd.style.backgroundColor = 'orange';
    } else if (enoughRegex.test(pwd.value)) {
        pwd.style.backgroundColor = 'red';
    } else {
        pwd.style.backgroundColor = 'white';
    }
}

function init(formId) {
    formFields = document.querySelectorAll(`#${formId} > .custom-input`);          // Get all input fields
    for (let i = 1; i < formFields.length; i++) {
        formFields[i].style.display = "none";                                      // Set display none to hide all inputs
    }
    formBtn = document.querySelector(`#${formId} button`);
    formBtn.style.display = "none";
}

function employeeFunction() {
    const header_span = document.getElementById("employee-input-heading");
    init("employee-form");

    if (flagEmployeeForm) {
        return;
    }

    let name = null;

    for (let i = 0; i < formFields.length - 1; i++) {
        let element = formFields[i];
        element.addEventListener("keypress", function (event) {
            if (element.value.length != 0
                && event.keyCode == 13 && validate(element.type, element.value)) { // If enter is pressed and data is entered
                let nextElement = element.nextElementSibling;
                let msg = `Can we know your ${nextElement.name}?`;
                console.log(msg);

                if (!name) {
                    name = element.value;                                       // Show Hello + <name>
                    msg = `Hi ${name}, ${msg}`;
                }

                header_span.innerHTML = `${msg}`;
                nextElement.style.display = "block";                            // Show next element and focus
                element.style.display = "none";
                nextElement.focus();

                if (i == formFields.length - 2) {
                    formBtn.style.display = "block";
                    formBtn.addEventListener("click", function () {
                        header_span.innerHTML = "Thanks for telling us about yourself, Your ID is: 1";
                        flagEmployeeForm = true;
                        setTimeout(function () {
                            $('#menu-checkbox-employee').click();
                            vehicleFunction();
                        }, 0);
                    });
                }
            }
        });

        element.addEventListener('select', function (event) {
            if (element.value.length != 0
                && event.keyCode == 13 && validate(element.type, element.value)) {                                       // If enter is pressed and data is entered
                let nextElement = element.nextElementSibling;
                let msg = `Can we know your ${nextElement.name}?`;
                console.log(msg);

                if (!name) {
                    name = element.value;                                       // Show Hello + <name>
                    msg = `Hi ${name}, ${msg}`;
                }

                header_span.innerHTML = `${msg}`;
                nextElement.style.display = "block";                            // Show next element and focus
                element.style.display = "none";
                nextElement.focus();

                if (i == formFields.length - 2) {
                    formBtn.style.display = "block";
                    formBtn.addEventListener("click", function () {
                        header_span.innerHTML = "Thanks for telling us about yourself, Your ID is: 1";
                        flagEmployeeForm = true;
                        setTimeout(function () {
                            $('#menu-checkbox-employee').click();
                            vehicleFunction();
                        }, 0);
                    });
                }
            }
        });
    }
}

function vehicleFunction() {
    const header_span = document.getElementById("vehicle-input-heading");
    if (!flagEmployeeForm) {
        alert("Please fill the employee form first.");
        $('#menu-checkbox-vehicle').click();
        return;
    }
    init("vehicle-form");

    if (flagVehicleForm) {
        return;
    }

    $('#menu-checkbox-vehicle').click();

    for (let i = 0; i < formFields.length - 1; i++) {
        let element = formFields[i];
        element.addEventListener("keyup", function (event) {
            if (element.value.length != 0
                && event.keyCode == 13) {                                       // If enter is pressed and data is entered
                let nextElement = element.nextElementSibling;
                let msg = `Can we know your ${nextElement.name}?`;
                console.log(msg);

                header_span.innerHTML = `${msg}`;
                nextElement.style.display = "block";                            // Show next element and focus
                element.style.display = "none";
                nextElement.focus();

                if (element.id == "vehicle-type") {
                    vehicleType = element.value;
                }

                if (i == formFields.length - 2) {
                    formBtn.style.display = "block";
                    formBtn.addEventListener("click", function () {
                        header_span.innerHTML = "Thanks for telling us about your vehicle";
                        flagVehicleForm = true;
                        setTimeout(function () {
                            $('#menu-checkbox-vehicle').click();
                            pricingFunction();
                        }, 0);
                    });
                }
            }
        });
    }
}

function pricingFunction() {
    if (!flagVehicleForm) {
        alert("Please fill the employee and vehicle form first.");
        $('#menu-checkbox-pricing').click();
        return;
    }
    $('#menu-checkbox-pricing').click();

    vehicles = document.querySelectorAll(`.vehicle-type`);
    for (let i = 0; i < vehicles.length; i++) {
        if (vehicles[i] != document.getElementById(`${vehicleType}-div`))
            vehicles[i].style.display = "none";                       // Set display none to hide all inputs
    }

    document.getElementById(`${vehicleType}-button`).display = "none";

    flagPricing = true;
}

function purchase(event) {
    var id = event.id;
    var currencySymbol = currency == "dollar" ? "$" : "¥";

    var selectedIndex = document.getElementById(id).selectedIndex;
    var selectedOption = document.getElementById(id).options[selectedIndex];

    var price = selectedOption.dataset.price;
    if (currencySymbol != "$")
        price = price / 2;

    document.getElementById(`${id}-price`).textContent = `${currencySymbol}${price}`;
    document.getElementById(`${id}-duration`).textContent = `/${selectedOption.dataset.duration}`;

    var btn = document.getElementById(`${id}-button`);
    btn.display = "block";
    console.log(btn);

    btn.addEventListener("click", function () {
        document.getElementById(`${id}-price`).textContent = `${currencySymbol}${price}`;
        document.getElementById(`${id}-duration`).textContent = "Final Price";
    });
}

function changeCurrency() {
    if (currency == "dollar")
        currency = "yen";
    else
        currency = "dollar";
}


function validate(inputType, value) {
    switch (inputType) {
        case "text":
            if (value.length < 2 || isNumeric(value)) {
                alert(`Enter valid Name`);
                return false;
            }
            break;

        case "password":
            if (!password) {
                password = value;
            } else {
                if (password === value) {
                    return true;
                } else {
                    alert(`password doesn't match`);
                    return false;
                }
            }

            if (value.length < 8 || value.search(/[0-9]/) < 0 || value.search(/[a-z]/i) < 0 || value.search(/[A-Z]/i) < 0) {
                alert(`Password should be in proper format`);
                return false;
            }
            break;

        case "email":
            var atposition = value.indexOf("@");
            var dotposition = value.lastIndexOf(".");
            if (atposition < 1 || dotposition < atposition + 2 || dotposition + 2 >= value.length) {
                alert("Please enter a valid e-mail address");
                return false;
            }
            break;

        case "number":
            if (!isNumeric(value) || value.length < 8) {
                alert("Please enter a valid e-mail address");
                return false;
            }


        case "gender":
            if (value)
                return false;

    }
    return true;
}


