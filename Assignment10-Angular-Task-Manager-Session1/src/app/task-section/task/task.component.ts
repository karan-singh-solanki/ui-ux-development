import { Component, OnInit, Inject, Input } from '@angular/core';
import { taskStructure } from 'src/app/taskStructure';

@Component({
  selector: 'app-task',
  templateUrl: './task.component.html',
  styleUrls: ['./task.component.css']
})

export class TaskComponent{

  @Input() currentTask!: taskStructure;
  showmodal= false;

  edit(){
    this.showmodal = !this.showmodal;
  }
}
