import { Component, Input, OnInit } from '@angular/core';
import { DataService } from '../data.service';
import { taskStructure } from '../taskStructure';

@Component({
  selector: 'app-task-section',
  templateUrl: './task-section.component.html',
  styleUrls: ['./task-section.component.css']
})
export class TaskSectionComponent {

  taskStatusList = ["New", "In Progress", "Completed"];
  tasks: taskStructure[];
  currentTask!: taskStructure;

  taskStatusListIds = ["cdk-drop-list-0", "cdk-drop-list-1", "cdk-drop-list-2"];
  static count: number = 1;
  @Input() newTaskAdded!: taskStructure;

  constructor(private dataService: DataService) {
    this.tasks = this.dataService.tasks;
  }

  getTasksOfStatus(status: string): taskStructure[] {
    return this.tasks.filter((task) => task.status == status);
  }

  drop(event: any) {
    let task: taskStructure = event.previousContainer.data[event.previousIndex]
    console.log("to " + event.container.id)
    if (event.previousContainer.id != event.container.id) {
      switch (event.container.id) {
        case "cdk-drop-list-0":
          task.status = "New"
          task.completionDate = ''
          break;
        case "cdk-drop-list-1":
          task.status = "In Progress"
          task.completionDate = ''
          break;
        case "cdk-drop-list-2":
          task.status = "Completed"
          task.completionDate = new Date().toDateString()
          break;
      }
      this.dataService.updateTask(task)
    }
  }
}
