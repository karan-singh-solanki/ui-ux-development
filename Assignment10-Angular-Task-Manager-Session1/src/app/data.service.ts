import { Injectable } from '@angular/core';
import { taskStructure } from './taskStructure';

@Injectable({
  providedIn: 'root'
})
export class DataService {
  tasks: taskStructure[] = [
    {
      id: 1,
      title: "Task 1",
      description: "Desc of task1",
      status: "New",
      creationDate: "Sun Sep 5 2021",
      priority: "low",
      completionDate: ""
    },

    {
      id: 2,
      title: "Task 2",
      description: "Desc of task2",
      status: "In Progress",
      creationDate: "Fri Aug 27 2021",
      priority: "high",
      completionDate: ""
    },

    {
      id: 3,
      title: "Task 3",
      description: "Desc of task3",
      status: "Completed",
      creationDate: "Wed Sep 1 2021",
      priority: "medium",
      completionDate: new Date().toDateString()
    }
  ];

  constructor() { }

  deleteTask(currentTask: taskStructure): void {
    console.log("Delete task called: " + currentTask.title);

    this.tasks.forEach((element, index) => {
      if (element.id == currentTask.id) this.tasks.splice(index, 1);
    });
  }

  updateTask(currentTask: taskStructure): void {
    console.log(currentTask);
    this.tasks.forEach((element, index) => {
      if (element.id == currentTask.id) {
        this.tasks.splice(index, 1)
        this.tasks.splice(index, 0, currentTask);
      }
    });
  }
}
