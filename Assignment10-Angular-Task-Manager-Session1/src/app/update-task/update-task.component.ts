import { AfterViewInit, Component, Input, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { DataService } from '../data.service';
import { taskStructure } from '../taskStructure';

@Component({
  selector: 'app-update-task',
  templateUrl: './update-task.component.html',
  styleUrls: ['./update-task.component.css']
})
export class UpdateTaskComponent implements AfterViewInit{

  model!: NgbModalRef;
  @Input() selectedTask!:taskStructure;
  updateTaskForm!:FormGroup;

  constructor(private modalService: NgbModal, private dataservice: DataService) {
    this.updateTaskForm = new FormGroup({
      id: new FormControl(''),
      title: new FormControl('', [Validators!.required, Validators.minLength(2)]),
      description: new FormControl(''),
      priority: new FormControl(''),
      status: new FormControl(''),
      creationDate: new FormControl(''),
      completionDate: new FormControl('')
    });
  }

  ngAfterViewInit(): void {
    this.updateForm();
  }

  open(content:any) { 
    this.modalService.open(content, {ariaLabelledBy: 'modal-title'}).result.then((result) => {
     this.selectedTask = this.updateTaskForm.value;
      if(result=="delete"){
        this.dataservice.deleteTask(this.selectedTask);
      }else if(result=="update"){
        if(this.selectedTask.status=="Completed")
          this.selectedTask.completionDate = new Date().toDateString()
        this.dataservice.updateTask(this.selectedTask)
        console.log("Clicked else "+result)
      }
    }, (reason) => {
     console.log(`Dismissed ${reason}`);
    });
   
  
   
    console.log("Clicked "+this.dataservice.tasks)
  }

  updateForm(){
    this.updateTaskForm.setValue({id: this.selectedTask.id, title: this.selectedTask.title,
      description: this.selectedTask.description,
      priority: this.selectedTask.priority,
      status: this.selectedTask.status,
      creationDate: this.selectedTask.creationDate,
      completionDate: this.selectedTask.completionDate
    });
  }


}
