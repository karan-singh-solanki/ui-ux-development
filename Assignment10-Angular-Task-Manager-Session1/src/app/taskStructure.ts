export interface taskStructure {
    id: number;
    title: string;
    description: string;
    status: string,
    creationDate: string,
    priority: string,
    completionDate:string
}