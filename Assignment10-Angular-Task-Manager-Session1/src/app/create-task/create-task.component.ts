import { Component } from '@angular/core';
import {FormControl, FormGroup, Validators } from '@angular/forms';
import {NgbModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import { DataService } from '../data.service';

@Component({
  selector: 'app-create-task',
  templateUrl: './create-task.component.html',
  styleUrls: ['./create-task.component.css']
})
export class CreateTaskComponent {
  model!: NgbModalRef;
  static count=10;

  newTaskForm:FormGroup =new FormGroup({
    id: new FormControl(CreateTaskComponent.count++),
    title: new FormControl('', [Validators!.required, Validators.minLength(2)]),
    description: new FormControl('',[Validators!.required]),
    priority: new FormControl('',[Validators!.required]),
    status: new FormControl('New'),
    creationDate: new FormControl(new Date().toDateString()),
    completionDate: new FormControl('')
  });

  constructor(private modalService: NgbModal, private dataservice: DataService) {
  }
  
  setData(){
    this.dataservice.tasks.push(this.newTaskForm.value);
  }

  open(content:any) {
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      console.log("result: "+result)
      this.setData()
      this.newTaskForm = new FormGroup({
        id: new FormControl(CreateTaskComponent.count++),
        title: new FormControl('', [Validators!.required, Validators.minLength(2)]),
        description: new FormControl('',[Validators!.required]),
        priority: new FormControl('',[Validators!.required]),
        status: new FormControl('New'),
        creationDate: new FormControl(new Date().toDateString()),
        completionDate: new FormControl('')
      });
    }, (reason) => {
      console.log(`Dismissed ${reason}`);
     });
  }

}
